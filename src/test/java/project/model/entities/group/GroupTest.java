package project.model.entities.group;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.Categories;
import project.model.entities.shared.*;

import static org.junit.jupiter.api.Assertions.*;

class GroupTest {

    private PersonID newPersonID1;
    private PersonID newPersonID2;
    private PersonID newPersonID3;
    private PersonID newPersonID4;
    private LedgerID newLedgerPersonID1;
    private LedgerID newLedgerPersonID2;
    private LedgerID newLedgerGroupID1;
    private LedgerID newLedgerGroupID2;
    private AccountID newAccountID1;
    private GroupID newGroupID1;
    private GroupID newGroupID2;
    private Group newGroup;

    @BeforeEach
    public void init() {
        newPersonID1 = new PersonID("101@switch.pt");
        newPersonID2 = new PersonID("102@switch.pt");
        newPersonID3 = new PersonID("103@switch.pt");
        newPersonID4 = new PersonID("104@switch.pt");

        newGroupID1 = new GroupID("201");
        newGroupID2 = new GroupID("202");

        newLedgerPersonID1 = new LedgerID("301");
        newLedgerPersonID2 = new LedgerID("302");

        newLedgerGroupID1 = new LedgerID("401");
        newLedgerGroupID2 = new LedgerID("402");

        newAccountID1 = new AccountID("501");

    }

    /**
     * Test for setMembers
     * Happy Case
     */
    @Test
    void setMembersHappyCaseTest() {
        // Arrange
        Group group = new Group(newGroupID1, "friends", "2019-12-18", newPersonID1, newLedgerPersonID2);

        PersonsIDs expected = new PersonsIDs();
        expected.addPersonID(new PersonID("1@switch.pt"));
        expected.addPersonID(new PersonID("2@switch.pt"));
        group.setMembers(expected);

        // Act
        PersonsIDs actual = group.getMembersIDs();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for setCategories
     * Happy Case
     */
    @Test
    void setCategoriesHappyCaseTest() {
        // Arrange
        Group group = new Group(newGroupID1, "friends", "2019-12-18", newPersonID1, newLedgerPersonID2);

        Categories expected = new Categories();
        expected.addCategory("A");
        expected.addCategory("B");
        group.setCategories(expected);

        // Act
        Categories actual = group.getCategories();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for setAccountsIDs
     * Happy Case
     */
    @Test
    void setAccountsIDsHappyCaseTest() {
        // Arrange
        Group group = new Group(newGroupID1, "friends", "2019-12-18", newPersonID1, newLedgerPersonID2);

        AccountsIDs expected = new AccountsIDs();
        expected.addAccountID(new AccountID("1"));
        expected.addAccountID(new AccountID("2"));
        group.setAccountsIDs(expected);

        // Act
        AccountsIDs actual = group.getAccountsIDs();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for Group constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for Group Constructor - HappyCase")
    void GroupConstructorHappyCaseTest() {
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);
        assertTrue(newGroup instanceof Group);
    }

    /**
     * Test for setGroupDescription
     * Ensure exception is throw with null groupDescription
     */
    @Test
    @DisplayName("Test Method setGroupDescription - Ensure exception with null group description")
    void setGroupDescriptionEnsureExceptionWithNullTest() {
        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            newGroup = new Group(newGroupID1, null, "2019-12-18", newPersonID1, newLedgerPersonID2);
        });
    }

    /**
     * Test for setBirthDate
     * Ensure exception is thrown with null birthDate
     */
    @Test
    @DisplayName("Test setBirthDate - Ensure IllegalArgumentException with null creationDate")
    void setBirthDateEnsureExceptionWithNullCreationDateTest() {

        // Assert
        assertThrows(NullPointerException.class, () -> {
            newGroup = new Group(newGroupID1, "Test Group", null, newPersonID1, newLedgerPersonID2);
        });
    }

    /**
     * Test for addMember
     * Happy case - ensure if members are added to the membersList
     */
    @Test
    @DisplayName("Test addMember - ensure if members are added to the membersList - test")
    void addMemberHappyCaseTest() {
        // Arrange
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);

        // Act
        boolean result = newGroup.addMemberID(newPersonID2);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for addMember
     * Ensure false if already there
     */
    @Test
    @DisplayName("Test addMember - ensure false if already there")
    void addMemberEnsureFalseTest() {
        // Arrange
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);

        // Act
        boolean result = newGroup.addMemberID(newPersonID1);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for addManager
     * Happy case - ensure managers are added to the group's list of managers
     */
    @Test
    @DisplayName("Test addManager - ensure if managers are added to the group's list of managers - test")
    void addManagerHappyCaseTest() {
        // Arrange
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);

        // Act
        boolean result = newGroup.addManagerID(newPersonID2);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for addManager
     * Ensure false if already there
     */
    @Test
    @DisplayName("Test addManager - ensure false if already there")
    void addManagerEnsureFalseTest() {
        // Arrange
        newGroup = new Group(newGroupID1, "family", "2019-12-18", newPersonID1, newLedgerPersonID2);

        // Act
        boolean result = newGroup.addManagerID(newPersonID1);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for Test for addAccountID - Happy case
     * Group Account is created
     */
    @Test
    @DisplayName("Test for addAccount - Happy case")
    void addAccountHappyCaseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        AccountID id = new AccountID("5");

        // Act
        boolean expected = family.addAccount(id);

        // Arrange
        assertTrue(expected);
    }

    /**
     * Test for addAccountID
     * Ensure false if already there
     */
    @Test
    @DisplayName("Test for addAccount - Ensure false")
    void addAccountEnsureFalseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);

        family.addAccount(newAccountID1);

        // Act

        boolean expected = family.addAccount(newAccountID1);

        // Arrange
        assertFalse(expected);
    }

    /**
     * Test for addCategory
     * Happy case
     */
    @Test
    @DisplayName("Test for addCategory - True if category was added")
    void addCategoryHappyCaseTest() {
        // Arrange
        Group g1 = new Group(newGroupID1, "isep", "2019-07-10", newPersonID1, newLedgerGroupID1);

        // Act
        boolean result = g1.addCategory("health");

        // Assert
        assertTrue(result);
    }

    /**
     * Test for addCategory
     * Sad case
     */
    @Test
    @DisplayName("Test for addCategory - False since category already existed")
    void addCategorySadCaseEnsureFalseSinceCategoryAlreadyExistedTest() {
        // Arrange
        Group g1 = new Group(newGroupID1, "isep", "2019-07-10", newPersonID1, newLedgerGroupID1);

        // Act
        g1.addCategory("health");
        boolean result = g1.addCategory("health");

        // Assert
        assertFalse(result);
    }

    /**
     * Test for addCategory
     * Ensure Exception is thrown since category cant have null designation
     */
    @Test
    @DisplayName("Test for addCategory - Ensure exception is thrown- designation of category cant be null")
    void addCategoryEnsureExceptionTest() {
        // Arrange
        Group g1 = new Group(newGroupID1, "isep", "2019-07-10", newPersonID1, newLedgerGroupID1);

        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            g1.addCategory(null);
        });
    }

    /**
     * Test for hasMemberID
     * Happy Case
     */
    @Test
    @DisplayName("Test for hasMemberID - Happy Case")
    void hasMemberIDHappyCaseTest() {
        // Arrange
        Group newGroup = new Group(newGroupID1, "Grupo Teste", "2020-01-01", newPersonID4, newLedgerGroupID1);
        newGroup.addMemberID(newPersonID1);
        newGroup.addMemberID(newPersonID2);
        newGroup.addMemberID(newPersonID3);

        // Act
        boolean actual = newGroup.hasMemberID(newPersonID3);

        // Assert

        assertTrue(actual);
    }

    /**
     * Test for hasMemberID
     * Ensure false when personID given as parameter is not member of the group
     */
    @Test
    @DisplayName("Test for hasMemberID - Ensure False Test")
    void hasMemberIDHappyEnsureFalseTest() {
        // Arrange
        Group newGroup = new Group(newGroupID1, "Grupo Teste", "2020-01-01", newPersonID4, newLedgerGroupID1);
        newGroup.addMemberID(newPersonID1);
        newGroup.addMemberID(newPersonID2);

        // Act
        boolean actual = newGroup.hasMemberID(newPersonID3);

        // Assert

        assertFalse(actual);
    }

    /**
     * Test for hasManagerID
     * Happy Case
     */
    @Test
    @DisplayName("Test for hasManagerID - Happy Case")
    void hasManagerIDHappyCaseTest() {
        // Arrange
        Group newGroup = new Group(newGroupID1, "Grupo Teste", "2020-01-01", newPersonID4, newLedgerGroupID1);

        // Act
        boolean actual = newGroup.hasManagerID(newPersonID4);

        // Assert

        assertTrue(actual);
    }

    /**
     * Test for hasManagerID
     * Ensure false when personID given as parameter is not manager of the group
     */
    @Test
    @DisplayName("Test for hasMemberID - Ensure False Test")
    void hasManagerIDHappyEnsureFalseTest() {
        // Arrange
        Group newGroup = new Group(newGroupID1, "Grupo Teste", "2020-01-01", newPersonID4, newLedgerGroupID1);

        // Act
        boolean actual = newGroup.hasMemberID(newPersonID3);

        // Assert

        assertFalse(actual);
    }

    /**
     * Test for Override Equals - Happy case
     * Same attributes
     */
    @Test
    @DisplayName("Test for equals - Happy case")
    void equalsHappyCaseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);
        Group football = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);

        // Act
        boolean result = family.equals(football);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for Override Equals - Happy case
     * Different ledgerID
     */
    @Test
    @DisplayName("Test for Equals - Ensure true when id is the same and other attributes not")
    void EqualsEnsureTrueWithSameIdDifferentAttributesTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        Group football = new Group(newGroupID1, "Family", "2020-01-14", newPersonID1, newLedgerGroupID2);

        // Act
        boolean result = family.equals(football);

        // Assert
        assertTrue(result);
    }

    /**
     * Test for Override Equals
     * Ensure False with different ID's
     */
    @Test
    @DisplayName("Test for equals - ensure false with different IDs")
    void equalsEnsureFalseDifferentIDs() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);
        GroupID id2 = new GroupID("4");
        Group football = new Group(id2, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);

        // Act
        boolean result = family.equals(football);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for Override Equals
     * Ensure False with different ID's
     */
    @Test
    @DisplayName("Test for equals - ensure false with different Identities")
    void equalsEnsureFalseDifferentIdentities() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);
        GroupID id2 = new GroupID("4");
        Group football = new Group(id2, "Familia", "2020-01-14", newPersonID1, newLedgerPersonID1);

        // Act
        boolean result = family.sameAs(football);

        // Assert
        assertFalse(result);
    }

    /**
     * Test for Override Equals - Happy case
     */
    @Test
    @DisplayName("Test for notEquals - Happy case")
    void notEqualsCreationDateHappyCaseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Family", "2020-01-14", newPersonID1, newLedgerGroupID1);
        Group football = new Group(newGroupID2, "Family", "1920-01-14", newPersonID1, newLedgerGroupID2);

        // Act
        boolean result = family.equals(football);

        // Arrange
        assertFalse(result);
    }

    /**
     * Test for Override Equals -Same Object - Happy case
     */
    @Test
    @DisplayName("Test for equals - Same Object - Happy case")
    void equalsSameObjectHappyCaseTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);

        // Act
        boolean result = family.equals(family);

        // Arrange
        assertTrue(result);
    }

    /**
     * Test for Override Equals - null Object
     */
    @Test
    @DisplayName("Test for equals - null Object")
    void equalsNullObjectTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        Group football = null;

        // Act
        boolean result = family.equals(football);

        // Arrange
        assertFalse(result);
    }

    /**
     * Test for Override Equals - different class objects
     */
    @Test
    @DisplayName("Test for equals - Objects from different Classes")
    void equalsDifferentObjectClassesTest() {
        // Arrange
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);

        // Act
        boolean expected = family.equals(newPersonID1);

        // Arrange
        assertFalse(expected);
    }

    /**
     * Test for Group hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void hashCodeEnsureTrueTest() {
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        int expectedHash = 856190498;
        int actualHash = family.hashCode();
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for Group toString
     * Ensure true
     */
    @Test
    @DisplayName("Test for toString - Ensure true")
    void toStringEnsureTrueTest() {
        Group family = new Group(newGroupID1, "Familia", "2020-01-14", newPersonID1, newLedgerGroupID1);
        String expected = "Group{groupID=GroupID{groupID=201}, members=Persons{persons=[PersonID{personID=101@switch" +
                ".pt}]}, " +
                "managers=Persons{persons=[PersonID{personID=101@switch.pt}]}, ledger=LedgerID{ledgerID=401}," +
                " categories=Categories{categories=[]}, accountIDs=AccountsIDs{AccountsIDs=[]}," +
                " description=Description{description='Familia'}, " +
                "creationDate=CreationDate{creationDate='2020-01-14'}}";
        String result = family.toString();
        assertEquals(expected, result);
    }

    /**
     * Test for SetGroupIDNull
     */
    @Test
    @DisplayName("Throw exception if GroupID is null")
    void setGroupIdEnsureExceptionWithNull() {
        //Assert
        assertThrows(InvalidFieldException.class, () -> {
            newGroup = new Group(null, "Test Group", "2019-12-18", newPersonID1, newLedgerPersonID2);
        });
    }

    /**
     * Test for SetLedgerIDNull
     */
    @Test
    @DisplayName("Throw exception if LedgerID is null")
    void setLedgerIdEnsureExceptionWithNull() {
        //Assert
        assertThrows(InvalidFieldException.class, () -> {
            newGroup = new Group(newGroupID1, "Test Group", "2019-12-18", newPersonID1, null);
        });
    }

    /**
     * Test for hasAccountID
     * <p>
     * Group has the accountID
     * Happy case
     */
    @Test
    void hasAccountIDHappyCase() {
        //ARRANGE
        Group testGroup = new Group(newGroupID1, "test group", "2019-05-27", newPersonID1, newLedgerGroupID1);

        testGroup.addAccount(newAccountID1);

        //ACT
        boolean result = testGroup.hasAccountID(newAccountID1);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for hasAccountID
     * <p>
     * Group doesn't have the accountID
     * Sad case
     */
    @Test
    void hasAccountIDSadCase() {
        //ARRANGE
        Group testGroup = new Group(newGroupID1, "test group", "2019-05-27", newPersonID1, newLedgerGroupID1);

        //ACT
        boolean result = testGroup.hasAccountID(newAccountID1);

        //ASSERT
        assertFalse(result);
    }

    /**
     * Method GetLedgerID
     * Happy Case
     */
    @Test
    @DisplayName("getLedgerID - HappyCase")
    void getLedgerIDHappyCase() {
        //Arrange
        Group testGroup = new Group(newGroupID1, "test group", "2019-05-27", newPersonID1, newLedgerGroupID1);
        LedgerID expected = new LedgerID("401");
        //Act
        LedgerID result = testGroup.getLedgerID();
        //Assert
        assertEquals(expected, result);
    }

}