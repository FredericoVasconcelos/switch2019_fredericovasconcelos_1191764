package project.model.entities.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;

import static org.junit.jupiter.api.Assertions.*;

class AccountsIDsTest {

    /**
     * Test for constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for AccountsIDs constructor - HappyCase")
    void AccountsIDsConstructorHappyCaseTest() {
        AccountsIDs newAccountsIDs = new AccountsIDs();
        assertTrue(newAccountsIDs instanceof AccountsIDs);
    }

    /**
     * Test for addAccountID
     * Happy case boolean Test
     */
    @Test
    @DisplayName("Test for addAccountID - Happy Case")
    void addAccountIDBooleanTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        AccountID main = new AccountID("100");
        //Act
        boolean result = newAccountsIDs.addAccountID(main);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for addAccountID
     * Happy case toString Test
     */
    @Test
    @DisplayName("Test for addAccountID - toString Test")
    void addAccountIDToStringTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        AccountID main = new AccountID("100");
        newAccountsIDs.addAccountID(main);
        String expected = "AccountsIDs{AccountsIDs=[AccountID{accountID=100}]}";
        //Act
        String result = newAccountsIDs.toString();
        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for addAccountID
     * Ensure false if AccountID already exists
     */
    @Test
    @DisplayName("Test for addAccountID - ensure false if AccountID already exists")
    void EnsureFalseIfAccountIDAlreadyExistsTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        AccountID main = new AccountID("100");
        AccountID otherAccount = new AccountID("100");
        newAccountsIDs.addAccountID(main);
        //Act
        boolean result = newAccountsIDs.addAccountID(otherAccount);
        //Assert
        assertFalse(result);
    }


    /**
     * Test for addAccountID
     * Test Exception for null AccountID
     */
    @Test
    @DisplayName("Test for addAccountID - Exception for null AccountID")
    void addAccountIDNullTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        //Act
        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            newAccountsIDs.addAccountID(null);
        });
    }

    /**
     * Test for Equals
     * ensure same object
     */
    @Test
    @DisplayName("Test for Equals - Happy Case")
    void AccountsIDsEqualsHappyCaseTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        //Act
        boolean result = newAccountsIDs.equals(newAccountsIDs);
        //Arrange
        assertTrue(result);
    }

    /**
     * Test for Equals
     * ensure true with same Attributes
     */
    @Test
    @DisplayName("Test for Equals - Same Attributes")
    void AccountsIDsEqualsTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        AccountsIDs otherAccountsIDs = new AccountsIDs();
        //Act
        boolean result = newAccountsIDs.equals(otherAccountsIDs);
        //Arrange
        assertTrue(result);
    }

    /**
     * Test for Equals
     * Different class objects
     */
    @Test
    @DisplayName("Test for Equals - different class objects test")
    void AccountsIDsEqualsDifferentClassObjectsTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        AccountID main = new AccountID("100");
        //Act
        boolean result = newAccountsIDs.equals(main);
        //Arrange
        assertFalse(result);
    }

    /**
     * Test for Equals
     * Null Test
     */
    @Test
    @DisplayName("Test for Equals - null Test")
    void AccountsIDsEqualsNullTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        //Act
        boolean result = newAccountsIDs.equals(null);
        //Arrange
        assertFalse(result);
    }

    /**
     * Test for hashCode
     * Happy Case
     */
    @Test
    @DisplayName("Test for hashCode - Happy Case")
    void AccountsIDsHashCodeTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        int expected = 31;
        //Act
        int result = newAccountsIDs.hashCode();
        //Arrange
        assertEquals(expected, result);
    }

    /**
     * Test for toString
     * Empty Test
     */
    @Test
    @DisplayName("Test for hashCode - Empty Test")
    void AccountsIDsToStringEmptyTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        String expected = "AccountsIDs{AccountsIDs=[]}";
        //Act
        String result = newAccountsIDs.toString();
        //Arrange
        assertEquals(expected, result);
    }

    /**
     * Test for toString
     * Empty Test
     */
    @Test
    @DisplayName("Test for hashCode - Empty Test")
    void AccountsIDsToStringHappyCaseTest() {
        //Arrange
        AccountsIDs newAccountsIDs = new AccountsIDs();
        AccountID main = new AccountID("100");
        newAccountsIDs.addAccountID(main);
        String expected = "AccountsIDs{AccountsIDs=[AccountID{accountID=100}]}";
        //Act
        String result = newAccountsIDs.toString();
        //Arrange
        assertEquals(expected, result);
    }
}