package project.model.entities.ledger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.exceptions.InvalidFieldException;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

class TransactionTest {

    private AccountID smithsGroceriesAccountID;
    private AccountID zeCreditAccountID;
    private Category newCategory;

    @BeforeEach
    public void init() {
        smithsGroceriesAccountID = new AccountID("11111");
        zeCreditAccountID = new AccountID("22222");
        newCategory = new Category("Groceries");
    }

    /**
     * Test for Transaction Constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for the constructor - Happy Case")
    void TransactionConstructorHappyCaseTest() {
        // Arrange
        // Act
        Transaction groceriesJanuary = new Transaction("100.87", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        // Assert
        assertTrue(groceriesJanuary instanceof Transaction);
    }

    /**
     * Test for Transaction Constructor
     * Exception with amount = 0
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with amount = 0")
    void TransactionConstructorEnsureExceptionWithAmountZeroTest() {
        // Arrange
        // Act
        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            Transaction groceriesJanuary = new Transaction("0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                    "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        });
    }

    /**
     * Test for Transaction Constructor
     * Exception with null date
     */
    @Test
    @DisplayName("Test for the constructor with null date")
    void TransactionConstructorWithNullDateTest() {
        // Arrange
        LocalDateTime myLocalDateTime = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        String expectedDate = myLocalDateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Transaction expected = new Transaction("15.0", Type.DEBIT, expectedDate, "Groceries for January", newCategory
                , smithsGroceriesAccountID, zeCreditAccountID);
        // Act
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, null, "Groceries for January", newCategory
                , smithsGroceriesAccountID, zeCreditAccountID);
        // Assert
        assertEquals(expected, groceriesJanuary);
    }

    /**
     * Test for Transaction Constructor
     * Exception with null category
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with null category")
    void TransactionConstructorEnsureExceptionWithNullCategoryTest() {
        // Arrange
        // Act
        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            Transaction groceriesJanuary = new Transaction("10", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                    "January", null, smithsGroceriesAccountID, zeCreditAccountID);
        });

    }

    /**
     * Test for Transaction Constructor
     * Exception with null debitAccountID
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with null debitAccountID")
    void TransactionConstructorEnsureExceptionWithNullDebitAccountIDTest() {
        // Arrange
        // Act
        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            Transaction groceriesJanuary = new Transaction("10", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                    "January", newCategory, null, zeCreditAccountID);
        });
    }

    /**
     * Test for Transaction Constructor
     * Exception with null creditAccountID
     */
    @Test
    @DisplayName("Test for the constructor - Ensure exception with null creditAccountID")
    void TransactionConstructorEnsureExceptionWithNullCreditAccountIDTest() {
        // Arrange
        // Act
        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            Transaction groceriesJanuary = new Transaction("10", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                    "January", newCategory, smithsGroceriesAccountID, null);
        });
    }

    /**
     * Test for Transaction equals
     * Happy case
     */
    @Test
    @DisplayName("Test for the equals - HappyCase")
    void TransactionEqualsHappyCaseTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = groceriesJanuary;
        //Act
        //Assert
        assertTrue(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction equals
     * Null
     */
    @Test
    @DisplayName("Test for the equals - Null")
    void TransactionEqualsNullTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = null;
        //Act
        //Assert
        assertFalse(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction equals
     * Objects from different classes
     */
    @Test
    @DisplayName("Test for the equals - objects from different classes")
    void TransactionEqualDifferentClassObjectsComparisonTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        //Act
        //Assert
        assertNotEquals(groceriesJanuary, newCategory);
    }

    /**
     * Test for Transaction equals
     * Ensure fails with different amounts
     */
    @Test
    @DisplayName("Test for the equals - different amounts")
    void TransactionEqualsDifferentAmountsTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = new Transaction("15.1", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        //Assert
        assertFalse(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction equals
     * Ensure fails with different types
     */
    @Test
    @DisplayName("Test for the equals - different types")
    void TransactionEqualsDifferentTypesTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = new Transaction("15.0", Type.CREDIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        //Assert
        assertFalse(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction equals
     * Ensure fails with different dates
     */
    @Test
    @DisplayName("Test for the equals - different dates")
    void TransactionEqualsDifferentDatesTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = new Transaction("15.0", Type.DEBIT, "2020-01-09 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        //Assert
        assertFalse(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction equals
     * Ensure fails with different descriptions
     */
    @Test
    @DisplayName("Test for the equals - different descriptions")
    void TransactionEqualsDifferentDescriptionsTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "February", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        //Assert
        assertFalse(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction equals
     * Ensure fails with different categories
     */
    @Test
    @DisplayName("Test for the equals - different categories")
    void TransactionEqualsDifferentCategoriesTest() {
        //Arrange
        Category firstCategory = new Category("Groceries");
        Category secondCategory = new Category("Groceries");
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", firstCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", secondCategory, smithsGroceriesAccountID, zeCreditAccountID);

        //Assert
        assertTrue(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction equals
     * Ensure fails with different categories with different contents
     */
    @Test
    @DisplayName("Test for the equals - different categories with different contents")
    void TransactionEqualsDifferentCategoriesWithDifferentContentsTest() {
        //Arrange
        Category firstCategory = new Category("Groceries");
        Category secondCategory = new Category("Others");
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", firstCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", secondCategory, smithsGroceriesAccountID, zeCreditAccountID);

        //Assert
        assertFalse(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction equals
     * Ensure fails with different debit accounts
     */
    @Test
    @DisplayName("Test for the equals - different debit accounts")
    void TransactionEqualsDifferentDebitAccountsTest() {
        //Arrange
        AccountID andersonGroceriesAccountID = new AccountID("44444");
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, andersonGroceriesAccountID, zeCreditAccountID);

        //Assert
        assertFalse(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction equals
     * Ensure fails with different credit accounts
     */
    @Test
    @DisplayName("Test for the equals - different credit accounts")
    void TransactionEqualsDifferentCreditAccountsTest() {
        //Arrange
        AccountID quimCreditAccountID = new AccountID("44444");
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        Transaction groceriesFebruary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, quimCreditAccountID);

        //Assert
        assertFalse(groceriesJanuary.equals(groceriesFebruary));
    }

    /**
     * Test for Transaction hashcode
     * Happy case
     */
    @Test
    @DisplayName("Test for hashcode - Ensure true with same attributes")
    void TransactionHashCodeEnsureTrueTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2020-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        int expectedHash = -1338304260;

        //Act
        int actualHash = groceriesJanuary.hashCode();

        //Assert
        assertEquals(expectedHash, actualHash);
    }

    /**
     * Test for getAmount
     * Ensure true
     */
    @Test
    @DisplayName("Test for getAmount - Ensure true")
    void getAmountEnsureTrueTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        double expected = -15;

        // Act
        double actual = groceriesJanuary.getFactoredAmount();

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getAmount
     * Ensure false
     */
    @Test
    @DisplayName("Test for getAmount - Ensure false")
    void getAmountEnsureFalseTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        double expected = 15;

        // Act
        double actual = groceriesJanuary.getFactoredAmount();

        // Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getFactoredAmount method
     * Test with transaction type debit (amount returned should be negative)
     */
    @Test
    void getFactoredAmountDebitTransactionTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        double expected = -20;

        //Act
        double actual = groceriesJanuary.getFactoredAmount();

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for getFactoredAmount method
     * Test with transaction type credit (amount returned should be positive)
     */
    @Test
    void getFactoredAmountCreditTransactionTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        double expected = -20;

        //Act
        double actual = groceriesJanuary.getFactoredAmount();

        //Assert
        assertEquals(expected, actual);

    }

    /**
     * Test for hasAccountID
     * Ensure true when searched accountId is the transaction's credit account
     */
    @Test
    @DisplayName("Test for hasAccount - Ensure true")
    void hasAccountIDTrueCaseCreditAccountTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        boolean expected = true;

        // Act
        boolean actual = groceriesJanuary.hasAccountID(smithsGroceriesAccountID);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for hasAccountID
     * Ensure true when searched accountId is the transaction's debit account
     */
    @Test
    @DisplayName("Test for hasAccount - Ensure true")
    void hasAccountIDTrueCaseDebitAccountTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, zeCreditAccountID, smithsGroceriesAccountID);
        boolean expected = true;

        // Act
        boolean actual = groceriesJanuary.hasAccountID(smithsGroceriesAccountID);

        // Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for hasAccountID
     * Ensure false
     */
    @Test
    @DisplayName("Test for hasAccount - Ensure false")
    void hasAccountIDFalseCaseTest() {
        //Arrange
        AccountID jonasAccountID = new AccountID("44444");
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        boolean expected = true;

        // Act
        boolean actual = groceriesJanuary.hasAccountID(jonasAccountID);

        // Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for hasAccountID
     * Ensure accountID is valid
     */
    @Test
    @DisplayName("Test for hasAccount - Ensure accountID is valid")
    void hasAccountIDEnsureThrowsExceptionNullAccountIDCaseTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        boolean expected = false;

        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            boolean actual = groceriesJanuary.hasAccountID(null);
        });
    }

    /**
     * Test for hasDebitAccountID
     * Ensure the accountID is debit account ID
     */
    @Test
    @DisplayName("Test for hasDebitAccountID - Ensure accountID is debit account ID")
    void hasDebitAccountIDTrueCaseTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        //Assert
        boolean actual = groceriesJanuary.hasDebitAccountID(smithsGroceriesAccountID);

        //Act
        assertTrue(actual);
    }

    /**
     * Test for hasDebitAccountID
     * Ensure false if account ID is Credit
     */
    @Test
    @DisplayName("Test for hasDebitAccountID - Ensure credit account returns false")
    void hasDebitAccountIDButCreditAccountIsGivenCaseTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        boolean expected = true;

        //Assert
        boolean actual = groceriesJanuary.hasDebitAccountID(zeCreditAccountID);

        //Act
        assertFalse(actual);
    }

    /**
     * Test for hasDebitAccountID
     * Ensure exception is thrown when account ID is not valid
     */
    @Test
    @DisplayName("Test for hasDebitAccountID - Ensure exception is thrown when account ID is not valid")
    void hasDebitAccountIDNullAccountIDCaseTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            boolean actual = groceriesJanuary.hasDebitAccountID(null);
        });
    }

    /**
     * Test for hasCreditAccountID
     * Ensure the accountID is credit account ID
     */
    @Test
    @DisplayName("Test for hasCreditAccountID - Ensure account ID is creditID")
    void hasCreditAccountIDTrueCaseTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        //Assert
        boolean actual = groceriesJanuary.hasCreditAccountID(zeCreditAccountID);

        //Act
        assertTrue(actual);
    }

    /**
     * Test for hasCreditAccountID
     * Ensure returns false if accountID is debitAccountID
     */
    @Test
    @DisplayName("Test for hasCreditAccountID - Ensure debitAccountID returns false")
    void hasCreditAccountIDButDebitAccountIDIsGivenCaseTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        //Assert
        boolean actual = groceriesJanuary.hasCreditAccountID(smithsGroceriesAccountID);

        //Act
        assertFalse(actual);
    }

    /**
     * Test for hasDebitAccountID
     * Ensure exception is thrown when accountID is not valid
     */
    @Test
    @DisplayName("Test for hasCreditAccountID - Ensure exception is thrown when accountID is not valid")
    void hasCreditAccountIDNullAccountIDCaseTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("20", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);

        // Assert
        assertThrows(InvalidFieldException.class, () -> {
            boolean actual = groceriesJanuary.hasCreditAccountID(null);
        });
    }

    /**
     * Test for toString
     * Happy case
     */
    @Test
    @DisplayName("Test for toString - Happy case")
    void toStringTest() {
        //Arrange
        Transaction groceriesJanuary = new Transaction("15.0", Type.DEBIT, "2021-01-08 00:00:00", "Groceries for " +
                "January", newCategory, smithsGroceriesAccountID, zeCreditAccountID);
        String expected = "Transaction{amount=Amount{amount=15.0}, dateTime=DateTime{dateTime='2021-01-08T00:00'}, " +
                "type=Type{type=-1}, description=Description{description='Groceries for January'}, " +
                "category=Category{designation=Designation{designation='Groceries'}}, " +
                "debitAccountID=AccountID{accountID=11111}, creditAccountID=AccountID{accountID=22222}}";

        // Act
        String result = groceriesJanuary.toString();

        // Assert
        assertEquals(expected, result);
    }

}