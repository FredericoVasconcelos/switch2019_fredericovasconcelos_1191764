package project.dto;

import org.junit.jupiter.api.Test;
import project.model.entities.account.Account;
import project.model.entities.shared.AccountID;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountsAssemblerTest {

    /**
     * Constructor test
     */
    @Test
    void constructorTest() {
        //ARRANGE
        AccountsAssembler assembler = new AccountsAssembler();
        //ASSERT
        assertTrue(assembler instanceof AccountsAssembler);
    }

    /**
     * Test for method mapToDTO
     * Happy Case
     */
    @Test
    void mapToDTOHappyCaseTest() {
        //ARRANGE
        String accountID = "1";
        String denomination = "peace money";
        String description = "money for thoughts and prayers";

        List<Account> accounts = new ArrayList<>();
        Account account = new Account(new AccountID(accountID), denomination, description);
        accounts.add(account);

        AccountDTO accountDTO = new AccountDTO(denomination, description, accountID);
        List<AccountDTO> accountsDTOs = new ArrayList<>();
        accountsDTOs.add(accountDTO);

        AccountsDTO expectedDTO = new AccountsDTO(accountsDTOs);

        //ACT
        AccountsDTO actualDTO = AccountsAssembler.mapToDTO(accounts);

        //ASSERT
        assertEquals(expectedDTO, actualDTO);
    }
}