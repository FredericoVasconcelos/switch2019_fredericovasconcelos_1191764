package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@Transactional
@SpringBootTest
class GetFamilyGroupsAssemblerTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    GroupRepository groupRepository;

    @BeforeEach
    public void init() {

        // Family groups are A, G, H

        // Group A (Carlos, Carla, Carlitos, Maria | Carlos + Carla = Carlitos & Maria)
        PersonID carlosID = new PersonID("101@switch.pt");
        LedgerID carlosLedgerID = new LedgerID("201");
        Person carlos = new Person(carlosID, "carlos", "Rua Santa Catarina 35", "Porto", "1989-12-18", null, null, carlosLedgerID);

        PersonID carlaID = new PersonID("102@switch.pt");
        LedgerID carlaLedgerID = new LedgerID("202");
        Person carla = new Person(carlaID, "carla", "Rua Santa Catarina 35", "Porto", "1989-12-18", null, null, carlaLedgerID);

        PersonID carlitosID = new PersonID("103@switch.pt");
        LedgerID carlitosLedgerID = new LedgerID("203");
        Person carlitos = new Person(carlitosID, "carlitos", "Rua Santa Catarina 35", "Porto", "2009-12-18", carlosID, carlaID, carlitosLedgerID);

        PersonID mariaID = new PersonID("104@switch.pt");
        LedgerID mariaLedgerID = new LedgerID("204");
        Person maria = new Person(mariaID, "maria", "Rua Santa Catarina 35", "Porto", "2009-12-18", carlosID, carlaID, mariaLedgerID);

        personRepository.save(carlos);
        personRepository.save(carla);
        personRepository.save(carlitos);
        personRepository.save(maria);

        GroupID groupAID = new GroupID("301");
        LedgerID groupALedgerID = new LedgerID("401");
        Group groupA = new Group(groupAID, "general", "2019-12-18", carlosID, groupALedgerID);

        groupA.addMemberID(carlaID);
        groupA.addMemberID(carlitosID);
        groupA.addMemberID(mariaID);

        groupRepository.save(groupA);

        // Group B (Carlos, Carla, Carlao, Carlota | Carlos + Joao = Carlota & Carla + Joao = Carlao)
        PersonID joaoID = new PersonID("111@switch.pt");
        LedgerID joaoLedgerID = new LedgerID("211");
        Person joao = new Person(joaoID, "joao", "Rua Santa Catarina 35", "Porto", "1989-12-18", null, null, joaoLedgerID);

        PersonID carlaoID = new PersonID("112@switch.pt");
        LedgerID carlaoLedgerID = new LedgerID("222");
        Person carlao = new Person(carlaoID, "carla", "Rua Santa Catarina 35", "Porto", "1989-12-18", carlaID, joaoID, carlaoLedgerID);

        PersonID carlotaID = new PersonID("113@switch.pt");
        LedgerID carlotaLedgerID = new LedgerID("223");
        Person carlota = new Person(carlotaID, "carlota", "Rua Santa Catarina 35", "Porto", "2009-12-18", carlosID, joaoID, carlotaLedgerID);

        personRepository.save(joao);
        personRepository.save(carlao);
        personRepository.save(carlota);

        GroupID groupBID = new GroupID("311");
        LedgerID groupBLedgerID = new LedgerID("411");
        Group groupB = new Group(groupBID, "general", "2019-12-18", carlosID, groupBLedgerID);

        groupB.addMemberID(carlaID);
        groupB.addMemberID(carlaoID);
        groupB.addMemberID(carlotaID);

        groupRepository.save(groupB);

        // Group C (Carlitos | Carlos + Carla = Carlitos)
        GroupID groupCID = new GroupID("321");
        LedgerID groupCLedgerID = new LedgerID("421");
        Group groupC = new Group(groupCID, "general", "2019-12-18", carlitosID, groupCLedgerID);

        groupRepository.save(groupC);

        // Group D (Carlos, Carlitos, Maria | Carlos + Carla = Carlitos & Maria)
        GroupID groupDID = new GroupID("331");
        LedgerID groupDLedgerID = new LedgerID("431");
        Group groupD = new Group(groupDID, "general", "2019-12-18", carlosID, groupDLedgerID);

        groupD.addMemberID(carlitosID);
        groupD.addMemberID(mariaID);

        groupRepository.save(groupD);

        // Group E (Carla, Carlitos, Maria | Carlos + Carla = Carlitos & Maria)
        GroupID groupEID = new GroupID("341");
        LedgerID groupELedgerID = new LedgerID("441");
        Group groupE = new Group(groupEID, "general", "2019-12-18", carlaID, groupELedgerID);

        groupE.addMemberID(carlitosID);
        groupE.addMemberID(mariaID);

        groupRepository.save(groupE);

        // Group F (Carlitos, Maria | Carlos + Carla = Carlitos & Maria)
        GroupID groupFID = new GroupID("351");
        LedgerID groupFLedgerID = new LedgerID("451");
        Group groupF = new Group(groupFID, "general", "2019-12-18", carlitosID, groupFLedgerID);

        groupF.addMemberID(mariaID);

        groupRepository.save(groupF);

        // Group G (Carlos, Carla, Carlitos, Patricia | Carlos + Carla = Carlitos & Carlos + Patricia = Catarina)
        PersonID patriciaID = new PersonID("121@switch.pt");
        LedgerID patriciaLedgerID = new LedgerID("221");
        Person patricia = new Person(patriciaID, "patricia", "Rua Santa Catarina 35", "Porto", "1989-12-18", null, null, patriciaLedgerID);

        PersonID catarinaID = new PersonID("122@switch.pt");
        LedgerID catarinaLedgerID = new LedgerID("222");
        Person catarina = new Person(catarinaID, "catarina", "Rua Santa Catarina 35", "Porto", "1989-12-18", patriciaID, carlosID, catarinaLedgerID);

        personRepository.save(patricia);
        personRepository.save(catarina);

        GroupID groupGID = new GroupID("361");
        LedgerID groupGLedgerID = new LedgerID("461");
        Group groupG = new Group(groupGID, "general", "2019-12-18", carlaID, groupGLedgerID);

        groupG.addMemberID(carlosID);
        groupG.addMemberID(carlitosID);
        groupG.addMemberID(catarinaID);

        groupRepository.save(groupG);

        // Group G (Carlos, Carla, Carlitos, Pedro | Carlos + Carla = Carlitos & Carla + Pedro = Carina)
        PersonID pedroID = new PersonID("131@switch.pt");
        LedgerID pedroLedgerID = new LedgerID("231");
        Person pedro = new Person(pedroID, "pedro", "Rua Santa Catarina 35", "Porto", "1989-12-18", null, null, pedroLedgerID);

        PersonID carinaID = new PersonID("132@switch.pt");
        LedgerID carinaLedgerID = new LedgerID("232");
        Person carina = new Person(carinaID, "carina", "Rua Santa Catarina 35", "Porto", "1989-12-18", carlaID, pedroID, carinaLedgerID);

        personRepository.save(pedro);
        personRepository.save(carina);

        GroupID groupHID = new GroupID("371");
        LedgerID groupHLedgerID = new LedgerID("471");
        Group groupH = new Group(groupHID, "general", "2019-12-18", carlaID, groupHLedgerID);

        groupH.addMemberID(carlosID);
        groupH.addMemberID(carlitosID);
        groupH.addMemberID(carinaID);

        groupRepository.save(groupH);

        // Group I (Carlos, Carla, Carlitos, Catia | Carlos + Carla = Carlitos & Pedro + Patricia = Catia)
        PersonID catiaID = new PersonID("141@switch.pt");
        LedgerID catiaLedgerID = new LedgerID("241");
        Person catia = new Person(catiaID, "catia", "Rua Santa Catarina 35", "Porto", "1989-12-18", patriciaID, pedroID, catiaLedgerID);

        personRepository.save(catia);

        GroupID groupIID = new GroupID("381");
        LedgerID groupILedgerID = new LedgerID("481");
        Group groupI = new Group(groupIID, "general", "2019-12-18", carlaID, groupILedgerID);

        groupI.addMemberID(carlosID);
        groupI.addMemberID(carlitosID);
        groupI.addMemberID(catiaID);

        groupRepository.save(groupI);

        // Group J (Carlos, Carla, Catarina, Carina | Carlos + Patricia = Catarina & Carla + Pedro = Carina)
        GroupID groupJID = new GroupID("391");
        LedgerID groupJLedgerID = new LedgerID("491");
        Group groupJ = new Group(groupJID, "general", "2019-12-18", carlaID, groupJLedgerID);

        groupJ.addMemberID(carlosID);
        groupJ.addMemberID(carlitosID);
        groupJ.addMemberID(catiaID);

        groupRepository.save(groupJ);
    }

    /**
     * Test for GetFamilyGroupsAssembler Constructor
     * Happy Case
     */
    @Test
    @DisplayName("Test for GetFamilyGroupsAssembler Constructor - Happy Case")
    void instanceOfGetFamilyGroupsAssembler() {
        GetFamilyGroupsAssembler assembler = new GetFamilyGroupsAssembler();
        assertTrue(assembler instanceof GetFamilyGroupsAssembler);
    }

    /**
     * Test for createDTOFromDomainObject Method
     * Happy case
     */
    @Test
    void createDTOFromDomainObjectHappyCase() {
        //Arrange
        GroupID groupAID = new GroupID("301");
        GroupID groupGID = new GroupID("361");
        GroupID groupHID = new GroupID("371");

        Set<GroupID> groupsIDs = new HashSet<>();
        groupsIDs.add(groupAID);
        groupsIDs.add(groupGID);
        groupsIDs.add(groupHID);

        Set<String> expectedString = new HashSet<>();
        expectedString.add("301");
        expectedString.add("361");
        expectedString.add("371");

        GetFamilyGroupsDTO expected = new GetFamilyGroupsDTO(expectedString);

        //Act
        GetFamilyGroupsDTO result = GetFamilyGroupsAssembler.mapToDTO(groupsIDs);

        //Assert
        assertEquals(expected, result);
    }

    /**
     * Test for createDTOFromDomainObject Method
     * Fail case
     */
    @Test
    void createDTOFromDomainObjectFailCase() {
        //Arrange
        GroupID groupAID = new GroupID("301");
        GroupID groupGID = new GroupID("361");
        GroupID groupHID = new GroupID("371");

        Set<GroupID> groupsIDs = new HashSet<>();
        groupsIDs.add(groupAID);
        groupsIDs.add(groupGID);
        groupsIDs.add(groupHID);

        Set<String> expectedString = new HashSet<>();
        expectedString.add("301");
        expectedString.add("361");
        expectedString.add("372");

        GetFamilyGroupsDTO expected = new GetFamilyGroupsDTO(expectedString);

        //Act
        GetFamilyGroupsDTO result = GetFamilyGroupsAssembler.mapToDTO(groupsIDs);

        //Assert
        assertNotEquals(expected, result);
    }

}