package project.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateTransactionForGroupResponseDTOTest {

    /**
     * Test for CreateTransactionForGroupResponseDTO constructor
     */
    @Test
    void constructorTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        //Act
        //Assert
        assertTrue(responseDTO instanceof CreateTransactionForGroupResponseDTO);

    }

    /**
     * Test for getGroupID method
     * Happy Case
     */
    @Test
    void getGroupIDHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "100";

        //Act
        String actual = responseDTO.getGroupID();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getGroupID method
     * Ensure not equals
     */
    @Test
    void getGroupIDEnsureNotEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "200";

        //Act
        String actual = responseDTO.getGroupID();

        //Assert
        assertNotEquals(expected, actual);

    }

    /**
     * Test for getGroupDescription method
     * Happy Case
     */
    @Test
    void getGroupDescriptionHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "group description";

        //Act
        String actual = responseDTO.getGroupDescription();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getGroupDescription method
     * Ensure not equals
     */
    @Test
    void getGroupDescriptionEnsureNotEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "different group description";

        //Act
        String actual = responseDTO.getGroupDescription();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getAmount method
     * Happy Case
     */
    @Test
    void getAmountHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "1000";

        //Act
        String actual = responseDTO.getAmount();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getAmount method
     * Ensure not equals
     */
    @Test
    void getAmountEnsureNotEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "2000";

        //Act
        String actual = responseDTO.getAmount();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Happy Case
     */
    @Test
    void getTypeHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "1";

        //Act
        String actual = responseDTO.getType();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Ensure not equals
     */
    @Test
    void getTypeEnsureNotEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "-1";

        //Act
        String actual = responseDTO.getType();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Happy Case
     */
    @Test
    void getTransactionDescriptionHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "transaction description";

        //Act
        String actual = responseDTO.getTransactionDescription();

        //Assert
        assertEquals(expected, actual);
    }

    /**
     * Test for getType method
     * Ensure not equals
     */
    @Test
    void getTransactionDescriptionEnsureNotEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String expected = "different transaction description";

        //Act
        String actual = responseDTO.getTransactionDescription();

        //Assert
        assertNotEquals(expected, actual);
    }

    /**
     * Test for equals method
     * Ensure true when comparing the same object
     */
    @Test
    void testEqualsEnsureTrueWhenComparingSameObjectTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        //Act
        boolean actual = responseDTO.equals(responseDTO);

        //Assert
        assertTrue(actual);
    }

    /**
     * Test for equals method
     * Ensure false when other object is null
     */
    @Test
    void testEqualsEnsureFalseWhenOtherObjectIsNullTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        CreateTransactionForGroupResponseDTO otherResponseDTO = null;

        //Act
        boolean actual = responseDTO.equals(otherResponseDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when other object is from different class
     */
    @Test
    void testEqualsEnsureFalseWhenOtherObjectIsFromDifferentClassTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String otherResponseDTO = "other";

        //Act
        boolean actual = responseDTO.equals(otherResponseDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure true when all attributes are equals
     */
    @Test
    void testEqualsEnsureTrueWhenAllAttributesAreEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        CreateTransactionForGroupResponseDTO otherResponseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        //Act
        boolean actual = responseDTO.equals(otherResponseDTO);

        //Assert
        assertTrue(actual);
    }

    /**
     * Test for equals method
     * Ensure false when groupID is different
     */
    @Test
    void testEqualsEnsureFalseWhenGroupIDIsDifferentTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupResponseDTO otherResponseDTO =
                new CreateTransactionForGroupResponseDTO(differentAttribute,
                groupDescription, amount, type, transactionDescription);

        //Act
        boolean actual = responseDTO.equals(otherResponseDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when groupDescription is different
     */
    @Test
    void testEqualsEnsureFalseWhenGroupDescriptionIsDifferentTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupResponseDTO otherResponseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                differentAttribute, amount, type, transactionDescription);

        //Act
        boolean actual = responseDTO.equals(otherResponseDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when amount is different
     */
    @Test
    void testEqualsEnsureFalseWhenAmountIsDifferentTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupResponseDTO otherResponseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, differentAttribute, type, transactionDescription);

        //Act
        boolean actual = responseDTO.equals(otherResponseDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when type is different
     */
    @Test
    void testEqualsEnsureFalseWhenTypeIsDifferentTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupResponseDTO otherResponseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, differentAttribute, transactionDescription);

        //Act
        boolean actual = responseDTO.equals(otherResponseDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for equals method
     * Ensure false when transactionDescription is different
     */
    @Test
    void testEqualsEnsureFalseWhenTransactionDescriptionIsDifferentTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        String differentAttribute = "different attribute";

        CreateTransactionForGroupResponseDTO otherResponseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, differentAttribute);

        //Act
        boolean actual = responseDTO.equals(otherResponseDTO);

        //Assert
        assertFalse(actual);
    }

    /**
     * Test for hashCode method
     */
    @Test
    void testHashCode() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";
        CreateTransactionForGroupResponseDTO responseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        int expected = -665169954;

        //Act
        int actual = responseDTO.hashCode();

        //Assert
        assertEquals(expected, actual);
    }
}