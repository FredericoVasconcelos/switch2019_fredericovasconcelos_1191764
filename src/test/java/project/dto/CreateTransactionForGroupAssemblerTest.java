package project.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CreateTransactionForGroupAssemblerTest {

    /**
     * Constructor test
     */
    @Test
    void constructorTest() {
        //Arrange
        CreateTransactionForGroupAssembler assembler = new CreateTransactionForGroupAssembler();

        //Act
        //Assert
        assertTrue(assembler instanceof CreateTransactionForGroupAssembler);
    }

    /**
     * Test for mapToRequestDTO method
     * Happy Case
     */
    @Test
    void mapToRequestDTOHappyCaseTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        CreateTransactionForGroupRequestDTO expectedRequestDTO = new CreateTransactionForGroupRequestDTO(personID,
                groupID,
                amount, dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        //Act
        CreateTransactionForGroupRequestDTO actualRequestDTO =
                CreateTransactionForGroupAssembler.mapToRequestDTO(personID, groupID, infoDTO);

        //Assert
        assertEquals(expectedRequestDTO, actualRequestDTO);

    }

    /**
     * Test for mapToRequestDTO method
     * Ensure not equals
     */
    @Test
    void mapToRequestDTOEnsureNotEqualsTest() {
        //Arrange
        String personID = "100";
        String groupID = "200";
        String amount = "1000";
        String dateTime = "2000-01-01 00 00 00";
        String type = "1";
        String description = "description";
        String categoryDesignation = "categoryDesignation";
        String debitAccountID = "1000";
        String creditAccountID = "2000";

        CreateTransactionInfoDTO infoDTO = new CreateTransactionInfoDTO(amount, dateTime, type, description,
                categoryDesignation, debitAccountID, creditAccountID);

        CreateTransactionForGroupRequestDTO expectedRequestDTO = new CreateTransactionForGroupRequestDTO(personID,
                groupID, "3000", dateTime, type, description, categoryDesignation, debitAccountID, creditAccountID);

        //Act
        CreateTransactionForGroupRequestDTO actualRequestDTO =
                CreateTransactionForGroupAssembler.mapToRequestDTO(personID, groupID, infoDTO);

        //Assert
        assertNotEquals(expectedRequestDTO, actualRequestDTO);

    }

    /**
     * Test for mapToResponseDTO
     * Happy Case
     */
    @Test
    void mapToResponseDTOHappyCaseTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";

        CreateTransactionForGroupResponseDTO expectedResponseDTO = new CreateTransactionForGroupResponseDTO(groupID,
                groupDescription, amount, type, transactionDescription);

        //Act
        CreateTransactionForGroupResponseDTO actualResponseDTO =
                CreateTransactionForGroupAssembler.mapToResponseDTO(groupID, groupDescription, amount, type,
                        transactionDescription);

        //Assert
        assertEquals(expectedResponseDTO, actualResponseDTO);
    }

    /**
     * Test for mapToResponseDTO
     * Ensure not equals
     */
    @Test
    void mapToResponseDTOEnsureNotEqualsTest() {
        //Arrange
        String groupID = "100";
        String groupDescription = "group description";
        String amount = "1000";
        String type = "1";
        String transactionDescription = "transaction description";

        CreateTransactionForGroupResponseDTO expectedResponseDTO = new CreateTransactionForGroupResponseDTO("5555",
                groupDescription, amount, type, transactionDescription);

        //Act
        CreateTransactionForGroupResponseDTO actualResponseDTO =
                CreateTransactionForGroupAssembler.mapToResponseDTO(groupID, groupDescription, amount, type,
                        transactionDescription);

        //Assert
        assertNotEquals(expectedResponseDTO, actualResponseDTO);
    }
}