package project.dto;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MembersDTOTest {

    /**
     * Test for MembersDTO constructor
     * Happy case
     */
    @DisplayName("Test for constructor - Happy case")
    @Test
    void constructorHappyCaseTest() {
        List<String> expectedMemberIds = new ArrayList<>();
        List<String> expectedMembersNames = new ArrayList<>();
        List<Boolean> expectedManagers = new ArrayList<>();
        
        MembersDTO dto = new MembersDTO(expectedMemberIds,expectedMembersNames,expectedManagers );

        //ASSERT
        assertTrue(dto instanceof MembersDTO);
    }

    /**
     * Test for getNames
     * Happy case
     */
    @DisplayName("Test for getNames Method")
    @Test
    void getNamesHappyCaseTest (){
        //ARRANGE
        List<String> membersIDS = new ArrayList<>();
        membersIDS.add("11@switch.pt");
        membersIDS.add("12@switch.pt");

        List<String> membersNames = new ArrayList<>();
        membersNames.add("diogo");
        membersNames.add("carlos");
        List<Boolean> whoIsManager = new ArrayList<>();

        MembersDTO dto = new MembersDTO(membersIDS,membersNames,whoIsManager);

        //ACT
        List<String> result = dto.getNames();

        //ASSERT
        assertEquals (membersNames, result);
    }

    /**
     * Test for getIsManager
     * Happy case
     */
    @DisplayName("Test for getIsManager Method")
    @Test
    void getIsManagerHappyCaseTest (){
        //ARRANGE
        List<String> membersIDS = new ArrayList<>();
        membersIDS.add("11@switch.pt");
        membersIDS.add("12@switch.pt");

        List<String> membersNames = new ArrayList<>();
        membersNames.add("diogo");
        membersNames.add("carlos");
        List<Boolean> whoIsManager = new ArrayList<>();
        whoIsManager.add(true);
        whoIsManager.add(false);

        MembersDTO dto = new MembersDTO(membersIDS,membersNames,whoIsManager);

        //ACT
        List<Boolean> result = dto.getIsManager();

        //ASSERT
        assertEquals (whoIsManager, result);
    }
    

    /**
     * Test for getMembersIDs
     * Happy case
     */
    @DisplayName("Test for getMembersIDs - Happy case")
    @Test
    void getMembersIDsHappyCaseTest() {
        //ARRANGE
        List<String> membersIDS = new ArrayList<>();
        membersIDS.add("11@switch.pt");
        membersIDS.add("12@switch.pt");

        List<String> membersNames = new ArrayList<>();
        List<Boolean> whoIsManager = new ArrayList<>();

        MembersDTO dto = new MembersDTO(membersIDS, membersNames,whoIsManager );

        //ACT
        List<String> result = dto.getMembersIDs();

        //ASSERT
        assertEquals(membersIDS, result);
    }

    /**
     * Test for Equals
     * Happy case
     */
    @DisplayName("Test for Equals - Happy case")
    @Test
    void equalsHappyCaseTest() {
        //ARRANGE
        MembersDTO expected;

        List<String> expectedMembersIDs = new ArrayList<>();
        List<String> membersNames = new ArrayList<>();
        List<Boolean> whoIsManager = new ArrayList<>();
        expectedMembersIDs.add("11@switch.pt");
        expectedMembersIDs.add("12@switch.pt");
        expected = new MembersDTO(expectedMembersIDs, membersNames, whoIsManager);

        MembersDTO dto;

        List<String> membersIDs = new ArrayList<>();
        List<String> membersNames2 = new ArrayList<>();
        List<Boolean> whoIsManager2 = new ArrayList<>();
        membersIDs.add("11@switch.pt");
        membersIDs.add("12@switch.pt");

        dto = new MembersDTO(membersIDs, membersNames2, whoIsManager2);

        //ACT
        boolean result = dto.equals(expected);

        //ASSERT
        assertTrue(result);
    }

    /**
     * Test for equals
     * Ensure false with null object
     */
    @Test
    @DisplayName("Test for Equals - Ensure false with null")
    void equalsEnsureFalseWithNullTest() {
        //Arrange
        List<String> expectedMembersIDs = new ArrayList<>();
        List<String> membersNames = new ArrayList<>();
        List<Boolean> whoIsManager = new ArrayList<>();
        MembersDTO dto1 = new MembersDTO(expectedMembersIDs, membersNames, whoIsManager);
        MembersDTO dto2 = null;
        //Act
        boolean result = dto1.equals(dto2);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure false with different objects
     */
    @Test
    @DisplayName("Test for Equals - Ensure false with different objects")
    void equalsEnsureFalseWithDifferentObjectsTest() {
        //Arrange
        List<String> expectedMembersIDs = new ArrayList<>();
        List<String> membersNames = new ArrayList<>();
        List<Boolean> whoIsManager = new ArrayList<>();
        MembersDTO dto1 = new MembersDTO(expectedMembersIDs, membersNames,whoIsManager);
        Set<String> expectedAccountsIDs = new HashSet<>();

        String string = "word";
        //Act
        boolean result = dto1.equals(string);
        //Assert
        assertFalse(result);
    }

    /**
     * Test for equals
     * Ensure true with same object
     */
    @Test
    @DisplayName("Test for Equals - Ensure true with same object")
    void equalsEnsureTrueWithSameObjectTest() {
        //Arrange
        List<String> expectedMembersIDs = new ArrayList<>();
        List<String> membersNames = new ArrayList<>();
        List<Boolean> whoIsManager = new ArrayList<>();
        MembersDTO dto = new MembersDTO(expectedMembersIDs, membersNames,whoIsManager);
        //Act
        boolean result = dto.equals(dto);
        //Assert
        assertTrue(result);
    }

    /**
     * Test for hashcode
     * Ensure works
     */
    @Test
    @DisplayName("Test for hashCode - Ensure works")
    void hashCodeEnsureWorksTest() {
        //Arrange
        List<String> expectedMembersIDs = new ArrayList<>();
        List<String> membersNames = new ArrayList<>();
        List<Boolean> whoIsManager = new ArrayList<>();
        expectedMembersIDs.add("11@switch.pt");
        expectedMembersIDs.add("12@switch.pt");
        MembersDTO dto = new MembersDTO(expectedMembersIDs,membersNames,whoIsManager);

        int expected = 1190285921;
        //Act
        int result = dto.hashCode();
        //Assert
        assertEquals(expected, result);
    }
}