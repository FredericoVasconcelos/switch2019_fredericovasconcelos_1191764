package project.dto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CreateCategoryDTOTest {

    CreateCategoryDTO dto;
    Set<String> designations;

    @BeforeEach
    void init() {
        designations = new HashSet<>();
        designations.add("designationA");
        designations.add("designationB");
        dto = new CreateCategoryDTO(designations);
    }

    @Test
    void constructorTest() {
        // Arrange
        // Act
        // Assert
        assertTrue(dto instanceof CreateCategoryDTO);
    }

    @Test
    void getDesignation() {
        // Arrange

        // Act
        Set<String> actual = dto.getDesignation();

        // Assert
        assertEquals(designations, actual);
    }

    @Test
    void setDesignation() {
        // Arrange
        Set<String> designations1 = new HashSet<>();
        designations1.add("designationC");
        dto.setDesignation(designations1);

        // Act
        Set<String> actual = dto.getDesignation();

        // Assert
        assertEquals(designations1, actual);
    }

    @Test
    void testEqualsEnsureTrueSameObject() {
        // Arrange
        // Act
        // Assert
        assertTrue(dto.equals(dto));
    }

    @Test
    void testEqualsEnsureTrueSameAttributes() {
        // Arrange
        CreateCategoryDTO dto1 = new CreateCategoryDTO(designations);
        // Act
        // Assert
        assertTrue(dto.equals(dto1));
    }

    @Test
    void testEqualsEnsureFalseNull() {
        // Arrange
        // Act
        // Assert
        assertFalse(dto.equals("bla"));
    }

    @Test
    void testEqualsEnsureFalseDifferentClass() {
        // Arrange
        // Act
        // Assert
        assertFalse(dto.equals(null));
    }

    @Test
    void testHashCode() {
        // Arrange
        int expected = -23182795;

        // Act
        int actual = dto.hashCode();

        // Assert
        assertEquals(expected, actual);
    }

    @Test
    void testToString() {
        // Arrange
        String expected = "CreateCategoryDTO{designation=[designationB, designationA]}";

        // Act
        String actual = dto.toString();

        // Assert
        assertEquals(expected, actual);
    }
}