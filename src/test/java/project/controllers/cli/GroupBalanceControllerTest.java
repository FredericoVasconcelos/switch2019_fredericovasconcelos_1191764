//package project.controllers;
//
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import project.model.domainentities.account.Account;
//import project.model.domainentities.group.Group;
//import project.model.domainentities.ledger.Type;
//import project.model.domainentities.person.Person;
//import project.model.domainentities.shared.Category;
//import project.model.domainentities.shared.Period;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//
//public class GroupBalanceControllerTest {
//
//    /**
//     * Test for group balance controller
//     * Happy Case
//     */
//    @Test
//    @DisplayName("Test for group balance controller- Happy case")
//    void groupBalanceControllerHappyCaseTest() {
//
//        //ARRANGE
//        GroupBalanceController control = new GroupBalanceController();
//        Person Diogo = new Person("Diogo", "rua do almada", "Porto", "1996-05-27", null, null);
//        Group g1 = new Group("groceries", "2018-01-26", Diogo);
//
//        Category c1 = new Category("school");
//        Category c2 = new Category("gym");
//
//        Account debit = new Account("water", "pay water bill");
//        Account credit = new Account("salary", "salary from work");
//        Account debit2 = new Account("health", "pay medical expenses");
//        Account credit2 = new Account("college", "award from school");
//
//        Period period = new Period("2020-02-24 00:00:00", "2020-03-26 00:00:00");
//
//        //ACT
//        g1.addCategory("school");
//        g1.addCategory("gym");
//        g1.addAccount("water", "pay water bill");
//        g1.addAccount("health", "pay medical expenses");
//        g1.addAccount("college", "award from school");
//        g1.addAccount("salary", "salary from work");
//        g1.addTransaction(20.50, Type.DEBIT, "2020-02-25 00:00:00", "music", c1, debit, credit);
//        g1.addTransaction(30.50, Type.DEBIT, "2020-03-25 00:00:00", "gym", c2, debit2, credit2);
//
//        double result = control.getBalanceGroupWithinPeriod(g1, period);
//        double expected = -51.00;
//
//        //ASSERT
//        assertEquals(expected, result);
//    }
//
//    /**
//     * Test for group balance controller
//     * Sad Case
//     */
//    @Test
//    @DisplayName("Test for group balance controller- Happy case")
//    void groupBalanceControllerSadCaseTest() {
//
//        //ARRANGE
//        GroupBalanceController control = new GroupBalanceController();
//        Person Diogo = new Person("Diogo", "rua do almada", "Porto", "1996-05-27", null, null);
//        Group g1 = new Group("groceries", "2018-01-26", Diogo);
//
//        Category c1 = new Category("school");
//        Category c2 = new Category("gym");
//
//        Account debit = new Account("water", "pay water bill");
//        Account credit = new Account("salary", "salary from work");
//        Account debit2 = new Account("health", "pay medical expenses");
//        Account credit2 = new Account("college", "award from school");
//
//        Period period = new Period("2020-02-24 00:00:00", "2020-03-26 00:00:00");
//
//        //ACT
//        g1.addCategory("school");
//        g1.addCategory("gym");
//        g1.addAccount("water", "pay water bill");
//        g1.addAccount("health", "pay medical expenses");
//        g1.addAccount("college", "award from school");
//        g1.addAccount("salary", "salary from work");
//        g1.addTransaction(20.50, Type.DEBIT, "2020-02-25 00:00:00", "music", c1, debit, credit);
//        g1.addTransaction(30.50, Type.DEBIT, "2020-03-25 00:00:00", "gym", c2, debit2, credit2);
//
//        double result = control.getBalanceGroupWithinPeriod(g1, period);
//        double expected = -45.00;
//
//        //ASSERT
//        assertNotEquals(expected, result);
//    }
//
//}
