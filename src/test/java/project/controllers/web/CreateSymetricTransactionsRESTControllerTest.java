package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.PersonLedgersIDsDTO;
import project.frameworkddd.IUSCreateSymetricTransactionService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class CreateSymetricTransactionsRESTControllerTest {

    @Autowired
    private IUSCreateSymetricTransactionService service;

    @Autowired
    private CreateSymetricTransactionRESTController controller;

    /**
     * Test for getLedgersByPersonID
     */
    @Test
    @DisplayName("Test for getLedgersByPersonID")
    public void getLedgersByPersonIDHappyCaseTest() {
        // Arrange
        String personIDStrg = "1001";
        List<String> expectedString = new ArrayList<>();
        expectedString.add("1001");
        expectedString.add("1002");
        expectedString.add("1004");
        PersonLedgersIDsDTO expectedOutputDTO = new PersonLedgersIDsDTO(expectedString);

        Mockito.when(service.getLedgersByPersonID(personIDStrg)).thenReturn(expectedOutputDTO);

        //ACT
        Object outputDTO = controller.getLedgersByPersonID(personIDStrg).getBody();

        //ASSERT
        assertEquals(expectedOutputDTO, outputDTO);
    }

}