package project.controllers.web;

import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GetPersonInfoRESTControllerIT extends AbstractTest {

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * Test for GetPeopleInfo
     * Happy Case
     *
     * @throws Exception
     */
    @Test
    @DisplayName("Test for GetPeopleInfo - Happy Case")
    public void GetPeopleInfoHappyCaseTest() throws Exception {
        String personID = "21001@switch.pt";

        Map<String, Object> pathVariableMap = new HashMap<>();
        pathVariableMap.put("personID", personID);

        String uri = UriComponentsBuilder.fromUriString("")
                .path("/people/{personID}")
                .buildAndExpand(pathVariableMap)
                .toUriString();

        int expectedStatus = 200;

        JSONObject jsonGroups = new JSONObject().put("href", "http://localhost/people/" + personID + "/groups");
        JSONObject jsonAccounts = new JSONObject().put("href", "http://localhost/people/" + personID + "/accounts");
        JSONObject jsonCategories = new JSONObject().put("href", "http://localhost/people/" + personID + "/categories");
        JSONObject jsonTransactions = new JSONObject().put("href", "http://localhost/people/" + personID + "/transactions");
        JSONObject jsonCreateGroup = new JSONObject().put("href", "http://localhost/people/" + personID + "/groups");

        JSONObject expectedContent = new JSONObject()
                .put("address", "Travessa Santa BÃ¡rbara")
                .put("birthplace", "Estarreja")
                .put("name", "JoÃ£o")
                .put("birthDate", "1987-04-17")
                .put("email", personID)
                .put("_links", new JSONObject()
                        .put("groupsURI", jsonGroups)
                        .put("accountsURI", jsonAccounts)
                        .put("categoriesURI", jsonCategories)
                        .put("transactionsURI", jsonTransactions)
                        .put("createGroupURI", jsonCreateGroup));

        // Act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
        int actualStatus = mvcResult.getResponse().getStatus();
        JSONObject actualContent = new JSONObject(mvcResult.getResponse().getContentAsString());

        // ASSERT
        assertEquals(expectedStatus, actualStatus);
        JSONAssert.assertEquals(expectedContent, actualContent, true);
    }
}
