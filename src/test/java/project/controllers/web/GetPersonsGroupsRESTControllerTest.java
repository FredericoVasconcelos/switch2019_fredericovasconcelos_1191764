package project.controllers.web;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GetPersonsGroupsResponseDTO;
import project.dto.GroupDTOMinimal;
import project.frameworkddd.IUSGetPersonsGroupsService;
import project.model.specifications.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
class GetPersonsGroupsRESTControllerTest extends AbstractTest {

    @Autowired
    private IUSGetPersonsGroupsService service;

    @Autowired
    private PersonRepository personRepo;

    @Autowired
    private GetPersonsGroupsRESTController controller;

    /**
     * Test for getPersonsGroups
     * Happy Case
     *
     * @throws Exception
     */
    @Test
    @DisplayName("Test for getPersonsGroups - Happy Case")
    public void getPersonsGroupsHappyCaseTest() throws Exception {
        // Arrange
        List<GroupDTOMinimal> expectedString = new ArrayList<>();
        expectedString.add(new GroupDTOMinimal("1", "description", "creationDate"));
        GetPersonsGroupsResponseDTO outputDTOExpected = new GetPersonsGroupsResponseDTO(expectedString);

        String personID = "1@switch.pt";
        Mockito.when(service.getPersonsGroups(personID)).thenReturn(outputDTOExpected);

        //ACT
        Object outputDTO = controller.getPersonsGroups(personID).getBody();

        //ASSERT
        assertEquals(outputDTOExpected, outputDTO);
    }

}