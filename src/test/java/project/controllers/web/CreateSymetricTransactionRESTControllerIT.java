package project.controllers.web;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import project.dto.CreateTransactionInfoDTO;
import project.model.specifications.repositories.LedgerRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateSymetricTransactionRESTControllerIT extends AbstractTest {

    @Autowired
    LedgerRepository ledgerRepository;

    @Override
    @BeforeAll
    public void setUp() {
        super.setUp();
    }

    /**
     * Test for method createTransactionForGroup
     * Happy Case - Transaction successfully created and added to group's ledger
     */
    @Transactional
    @Test
    void getLedgersByPersonIDHappyCaseTest() throws Exception {
        //Arrange
        String uriGetOwnedLedgersIDs = "/people/1001@switch.pt/ledgers";
        String expectedContent = "{\"ownedLedgersIDs\":[\"1001\",\"1002\",\"1004\"]}";
        int expectedStatus = 200;

        //Act
        MvcResult mvcResult = this.mvc.perform(MockMvcRequestBuilders.get(uriGetOwnedLedgersIDs)).andReturn();
        String actualContent = mvcResult.getResponse().getContentAsString();
        int actualStatus = mvcResult.getResponse().getStatus();

        //Assert
        assertEquals(expectedContent, actualContent);
        assertEquals(expectedStatus, actualStatus);

    }

}