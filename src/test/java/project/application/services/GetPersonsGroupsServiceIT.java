package project.application.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import project.ProjectApplication;
import project.dto.GetPersonsGroupsResponseDTO;
import project.dto.GroupDTOMinimal;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class GetPersonsGroupsServiceIT {

    @Autowired
    private GetPersonsGroupsService service;

    @Test
    void constructorHappyCaseTest() {
        // Arrange
        // Act
        // Assert
        assertTrue(service instanceof GetPersonsGroupsService);
    }

    /**
     * Test for GetPersonsGroupsService
     */
    @Test
    void getPersonsGroupsHappyCaseTest() {
        // Arrange
        List<GroupDTOMinimal> expectedGroupsDTOs = new ArrayList<>();
        expectedGroupsDTOs.add(new GroupDTOMinimal("4501", "general", "2019-12-18"));
        expectedGroupsDTOs.add(new GroupDTOMinimal("4502", "general", "2019-12-18"));
        expectedGroupsDTOs.add(new GroupDTOMinimal("4504", "general", "2019-12-18"));
        expectedGroupsDTOs.add(new GroupDTOMinimal("4507", "general", "2019-12-18"));
        expectedGroupsDTOs.add(new GroupDTOMinimal("4508", "general", "2019-12-18"));
        expectedGroupsDTOs.add(new GroupDTOMinimal("4509", "general", "2019-12-18"));
        expectedGroupsDTOs.add(new GroupDTOMinimal("4510", "general", "2019-12-18"));

        GetPersonsGroupsResponseDTO expected = new GetPersonsGroupsResponseDTO(expectedGroupsDTOs);

        // Act
        GetPersonsGroupsResponseDTO actual = service.getPersonsGroups("4001@switch.pt");

        // Assert
        assertEquals(expected, actual);
    }

}
