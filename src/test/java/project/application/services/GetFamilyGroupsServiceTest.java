package project.application.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.GetFamilyGroupsDTO;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.services.IsFamilyService;
import project.model.specifications.repositories.GroupRepository;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)

public class GetFamilyGroupsServiceTest {

    @Autowired
    private GroupRepository groupRepositoryDB;

    @Test
    @DisplayName("Test for getFamilyGroups - No family groups")
    void getFamilyGroupsNotFamilyGroupsTest() {

        //Arrange
        IsFamilyService mockedService = mock(IsFamilyService.class);
        GetFamilyGroupsService service = new GetFamilyGroupsService(groupRepositoryDB, mockedService);

        Set<String> familyGroupsIDsString = new HashSet<>();
        GetFamilyGroupsDTO expected = new GetFamilyGroupsDTO(familyGroupsIDsString);

        GroupID groupID1 = new GroupID("4004444");
        GroupID groupID2 = new GroupID("4004443");


        Group group1 = new Group(groupID1, "Familia Alves", "2020-03-03", new PersonID("111@switch.pt"), new LedgerID("444"));
        Group group2 = new Group(groupID2, "Familia Alves", "2020-03-03", new PersonID("113@switch.pt"), new LedgerID("446"));

        Set<Group> listOfAllGroups = new HashSet<>();
        listOfAllGroups.add(group1);
        listOfAllGroups.add(group2);

        Mockito.when(groupRepositoryDB.findAll()).thenReturn(listOfAllGroups);
        Mockito.when(mockedService.isFamily(group1)).thenReturn(false);
        Mockito.when(mockedService.isFamily(group2)).thenReturn(false);

        //Act
        GetFamilyGroupsDTO result = service.getFamilyGroups();

        //Assert
        assertEquals(expected, result);

    }

    @Test
    @DisplayName("Test for getFamilyGroups")
    void getFamilyGroupsTest() {
        //Arrange

        GroupID groupID1 = new GroupID("4004444");
        GroupID groupID2 = new GroupID("4004443");

        Group group1 = new Group(groupID1, "Familia Alves", "2020-03-03", new PersonID("111@switch.pt"), new LedgerID("444"));
        Group group2 = new Group(groupID2, "Familia Alves", "2020-03-03", new PersonID("113@switch.pt"), new LedgerID("446"));


        IsFamilyService mockedService = mock(IsFamilyService.class);
        GetFamilyGroupsService service = new GetFamilyGroupsService(groupRepositoryDB, mockedService);

        Set<String> familyGroupsIDsString = new HashSet<>();
        GetFamilyGroupsDTO expected = new GetFamilyGroupsDTO(familyGroupsIDsString);
        familyGroupsIDsString.add("4004444");

        Set<Group> listOfAllGroups = new HashSet<>();
        listOfAllGroups.add(group1);
        listOfAllGroups.add(group2);

        Mockito.when(groupRepositoryDB.findAll()).thenReturn(listOfAllGroups);
        Mockito.when(mockedService.isFamily(group1)).thenReturn(true);
        Mockito.when(mockedService.isFamily(group2)).thenReturn(false);

        //Act
        GetFamilyGroupsDTO result = service.getFamilyGroups();

        //Assert
        assertEquals(expected, result);

    }

}


