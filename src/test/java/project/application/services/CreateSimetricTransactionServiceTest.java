package project.application.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import project.dto.TransactionDTO;
import project.dto.TransactionsAssembler;
import project.dto.TransactionsResponseDTO;
import project.exceptions.*;
import project.frameworkddd.IUSGetTransactionsService;
import project.model.entities.Transactions;
import project.model.entities.account.Account;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Transaction;
import project.model.entities.ledger.Type;
import project.model.entities.person.Person;
import project.model.entities.shared.*;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("serviceTest")
@ExtendWith(SpringExtension.class)
class CreateSimetricTransactionServiceTest {

    @Autowired
    IUSGetTransactionsService service;

    @Autowired
    PersonRepository personRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    LedgerRepository ledgerRepository;


//    /**
//     * Unit test for method getTransactionsByPersonID
//     * Happy Case
//     */
//    @Test
//    @DisplayName("getTransactionsByPersonID - Happy Case Test")
//    void getTransactionsByPersonIDHappyCaseTest() {
//
//        //Variables
//        String personIdStg = "1001@switch.pt";
//        PersonID personID = new PersonID(personIdStg);
//
//        String ledgerGroupIdStg = "1001";
//        LedgerID personLedgerID = new LedgerID(ledgerGroupIdStg);
//
//        String initialDateTime = "1999-01-01 12:00:00";
//        String finalDateTime = "2021-01-01 12:00:00";
//
//        Type typeDebit = Type.DEBIT;
//        Type typeCredit = Type.CREDIT;
//
//        Category category = new Category("comida e bebida");
//
//        //TransactionsD for expectedTransactionSet
//        Transaction t1 = new Transaction("500", typeCredit, "2000-01-01 23:20:58", "Pacote de Oreos", category, debitAccountId, creditAccountId);
//        Transaction t2 = new Transaction("400", typeDebit, "2010-01-01 11:39:23", "Pacote de Belgas", category, debitAccountId, creditAccountId);
//
//        List<Transaction> expectedTransactionList = new ArrayList<>();
//        expectedTransactionList.add(t1);
//        expectedTransactionList.add(t2);
//
//
//        //TransactionsDTO for expectedTransactionDTOList to build expectedResponseDTO
//        TransactionDTO tdto1 = new TransactionDTO("500", "CREDIT", "2000-01-01 23:20:58", "Pacote de Oreos", "comida e bebida", "1002", "1004");
//        TransactionDTO tdto2 = new TransactionDTO("400", "DEBIT", "2010-01-01 11:39:23", "Pacote de Belgas", "comida e bebida", "1002", "1004");
//
//        List<TransactionDTO> expectedTransactionDTOList = new ArrayList<>();
//        expectedTransactionDTOList.add(tdto1);
//        expectedTransactionDTOList.add(tdto2);
//
//
//        //Mocks
//        Person mockPerson = Mockito.mock(Person.class);
//        Account mockAccount = Mockito.mock(Account.class);
//        Ledger mockLedger = Mockito.mock(Ledger.class);
//        Transactions mockTransactions = Mockito.mock(Transactions.class);
//
//
//        Mockito.when(personRepository.findById(personID)).thenReturn(Optional.of(mockPerson));
//        Mockito.when(mockPerson.getLedgerID()).thenReturn(personLedgerID);
//
//        //getTransactionsByLedgerID
//        Mockito.when(ledgerRepository.findById(personLedgerID)).thenReturn(Optional.of(mockLedger));
//        Mockito.when(mockLedger.getTransactions()).thenReturn(mockTransactions);
//        Mockito.when(mockTransactions.getAll()).thenReturn(expectedTransactionList);
//
//        //Expected Response DTO
//        TransactionsResponseDTO expectedResponseDTO = new TransactionsResponseDTO(expectedTransactionDTOList);
//
//        TransactionsResponseDTO actualResponseDTO = service.getTransactionsByPersonID(personIdStg);
//
//        assertEquals(expectedResponseDTO, actualResponseDTO);
//
//    }

}