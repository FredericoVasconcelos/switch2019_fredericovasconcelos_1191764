package project.application.services;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import project.ProjectApplication;
import project.dto.GetPersonsGroupsResponseDTO;
import project.dto.GroupDTOMinimal;
import project.dto.PersonLedgersIDsDTO;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(classes = ProjectApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CreateSimetricTransactionServiceIT {

    @Autowired
    private CreateSimetricTransactionService service;

    @Test
    void constructorHappyCaseTest() {
        // Arrange
        // Act
        // Assert
        assertTrue(service instanceof CreateSimetricTransactionService);
    }



    /**
     * Test for GetPersonsGroupsService
     */
    @Test
    void getPersonsGroupsHappyCaseTest() {
        // Arrange
        List<String> expectedStrings = new ArrayList<>();
        expectedStrings.add("1001");
        expectedStrings.add("1002");
        expectedStrings.add("1004");
        PersonLedgersIDsDTO expectedOutputDTO = new PersonLedgersIDsDTO(expectedStrings);

        // Act
        PersonLedgersIDsDTO actual = service.getLedgersByPersonID("1001@switch.pt");

        // Assert
        assertEquals(expectedOutputDTO, actual);
    }

}
