package project.exceptions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ApiErrorTest {

    /**
     * Test for constructor with no fields
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor with no fields - Happy case")
    void constructorWithNoFieldsTest() {
        //ARRANGE
        ApiError apiError = new ApiError();

        // ACT
        //ASSERT
        assertTrue(apiError instanceof ApiError);
    }

    /**
     * Test for constructor with single error
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor with single error - Happy case")
    void constructorSingleErrorTest() {
        //ARRANGE
        ApiError apiError = new ApiError(HttpStatus.OK, "message", "error");

        // ACT
        //ASSERT
        assertTrue(apiError instanceof ApiError);
    }

    /**
     * Test for constructor with multiple errors
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor with multiple errors - Happy case")
    void constructorMultipleErrorsTest() {
        //ARRANGE
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");

        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        // ACT
        //ASSERT
        assertTrue(apiError instanceof ApiError);
    }

    /**
     * Test for getStatus
     * Happy case
     */
    @Test
    @DisplayName("Test for getStatus - Happy case")
    void getStatusHappyCaseTest() {
        //ARRANGE
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        HttpStatus expected = HttpStatus.OK;

        // ACT
        HttpStatus result = apiError.getStatus();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getMessage
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage - Happy case")
    void getMessageHappyCaseTest() {
        //ARRANGE
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        String expected = "message";

        // ACT
        String result = apiError.getMessage();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for getErrors
     * Happy case
     */
    @Test
    @DisplayName("Test for getErrors - Happy case")
    void getErrorsHappyCaseTest() {
        //ARRANGE
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        List<String> expected = list;

        // ACT
        List<String> result = apiError.getErrors();

        //ASSERT
        assertEquals(expected, result);
    }

    /**
     * Test for equals method
     * Ensure true if this is other
     */
    @Test
    void testEqualsEnsureTrueIfThisIsOtherTest() {
        //Arrange
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        //Act
        boolean actual = apiError.equals(apiError);

        //Assert
        assertTrue(actual);

    }

    /**
     * Test for equals method
     * Ensure false when comparing this to null
     */
    @Test
    void testEqualsEnsureFalseWhenComparesWithNullObjectTest() {
        //Arrange
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        //Act
        boolean actual = apiError.equals(null);

        //Assert
        assertFalse(actual);

    }

    /**
     * Test for equals method
     * Ensure false when comparing this an object of another class
     */
    @Test
    void testEqualsEnsureFalseWhenComparesWithAnotherClassObjectTest() {
        //Arrange
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);
        String other = "other";

        //Act
        boolean actual = apiError.equals(other);

        //Assert
        assertFalse(actual);

    }

    /**
     * Test for equals method
     * Ensure true when all attributes are equal
     */
    @Test
    void testEqualsEnsureTrueWhenAllAttributesAreEqualTest() {
        //Arrange
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        ApiError otherApiError = new ApiError(HttpStatus.OK, "message", list);

        //Act
        boolean actual = apiError.equals(otherApiError);

        //Assert
        assertTrue(actual);

    }

    /**
     * Test for equals method
     * Ensure false when HttpStatus is different
     */
    @Test
    void testEqualsEnsureFalseWhenHttpStatusIsDifferentTest() {
        //Arrange
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        ApiError otherApiError = new ApiError(HttpStatus.CREATED, "message", list);

        //Act
        boolean actual = apiError.equals(otherApiError);

        //Assert
        assertFalse(actual);

    }

    /**
     * Test for equals method
     * Ensure false when message is different
     */
    @Test
    void testEqualsEnsureFalseWhenMessageIsDifferentTest() {
        //Arrange
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        ApiError otherApiError = new ApiError(HttpStatus.OK, "different message", list);

        //Act
        boolean actual = apiError.equals(otherApiError);

        //Assert
        assertFalse(actual);

    }

    /**
     * Test for equals method
     * Ensure false when list of messages is different
     */
    @Test
    void testEqualsEnsureFalseWhenMessageListIsDifferentTest() {
        //Arrange
        List<String> list = new ArrayList<>();
        list.add("error 1");
        list.add("error 2");
        ApiError apiError = new ApiError(HttpStatus.OK, "message", list);

        List<String> otherList = new ArrayList<>();
        list.add("error 111");
        list.add("error 222");
        ApiError otherApiError = new ApiError(HttpStatus.OK, "message", otherList);

        //Act
        boolean actual = apiError.equals(otherApiError);

        //Assert
        assertFalse(actual);

    }

}
