package project.exceptions;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccountAlreadyExistsExceptionTest {

    /**
     * Test for constructor filled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor filled - Happy case")
    void constructorFilledTest() {
        //ARRANGE
        AccountAlreadyExistsException ex = new AccountAlreadyExistsException("This is an error message");

        // ACT
        //ASSERT
        assertTrue(ex instanceof AccountAlreadyExistsException);
    }

    /**
     * Test for constructor unfilled
     * Happy case
     */
    @Test
    @DisplayName("Test for constructor unfilled - Happy case")
    void constructorUnfilledTest() {
        //ARRANGE
        AccountAlreadyExistsException ex = new AccountAlreadyExistsException();

        // ACT
        //ASSERT
        assertTrue(ex instanceof AccountAlreadyExistsException);
    }

    /**
     * Test for getMessage with filled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with filled constructor - Happy case")
    void getMessageFilledConstructorHappyCase() {
        //ARRANGE
        AccountAlreadyExistsException ex = new AccountAlreadyExistsException("This is an error message");
        String expected = "This is an error message";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

    /**
     * Test for getMessage with unfilled constructor
     * Happy case
     */
    @Test
    @DisplayName("Test for getMessage with unfilled constructor - Happy case")
    void getMessageUnfilledConstructorHappyCase() {
        //ARRANGE
        AccountAlreadyExistsException ex = new AccountAlreadyExistsException();
        String expected = "Account already exists";

        // ACT
        String actual = ex.getMessage();

        //ASSERT
        assertEquals(expected, actual);
    }

}