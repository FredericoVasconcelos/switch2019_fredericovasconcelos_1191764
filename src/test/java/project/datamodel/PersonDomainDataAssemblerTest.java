package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.entities.person.Person;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PersonDomainDataAssemblerTest {

    /**
     * Test for toData Method
     */
    @Test
    void toDataMethodTest() {

        //arrange
        PersonID newPersonID = new PersonID("12@switch.pt");
        LedgerID newLedgerID = new LedgerID("10");

        Person diogo = new Person(newPersonID, "diogo", "gaia", "porto", "1996-05-27", null, null, newLedgerID);

        //act
        PersonJpa expected = new PersonJpa("12@switch.pt", "diogo", "gaia", "porto", "1996-05-27", "10");
        PersonJpa result = PersonDomainDataAssembler.toData(diogo);


        //assert
        assertEquals(expected, result);
    }

//    /**
//     * Test for toDomainMethod
//     */
//    @Test
//    void toDomainMethodTest() {
//        //arrange
//        PersonJpa diogo = new PersonJpa("12", "diogo", "gaia", "porto", "1996-05-27", "10");
//
//        //act
//        Person expected = new Person(diogo.getId(), diogo.getName(), diogo.getAddress(), diogo.getBirthPlace(), diogo.getBirthDate(), null, null, diogo.getLedgerID());
//        Person result = PersonDomainDataAssembler.toDomain(diogo);
//
//        //assert
//        assertEquals(expected, result);
//    }

    /**
     * Test for toDomainMethod
     * Setting accountID for person
     */
//    @Test
//    void toDomainMethodTest1() {
//        //arrange
//        PersonJpa diogo = new PersonJpa("12", "diogo", "gaia", "porto", "1996-05-27", "10");
//
//        List<AccountID> accountIDS = new ArrayList<>();
//        AccountID accountID = new AccountID("12");
//        accountIDS.add(accountID);
//        diogo.setAccountsIds(accountIDS);
//
//        //act
//        Person expected = new Person(diogo.getId(), diogo.getName(), diogo.getAddress(), diogo.getBirthPlace(), diogo.getBirthDate(), null, null, diogo.getLedgerID());
//        expected.addAccount(accountID);
//        Person result = PersonDomainDataAssembler.toDomain(diogo);
//
//        //assert
//        assertEquals(expected, result);
//    }

}