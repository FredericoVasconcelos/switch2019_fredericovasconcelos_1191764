package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.entities.ledger.Amount;
import project.model.entities.ledger.Transaction;
import project.model.entities.ledger.Type;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.Description;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TransactionDomainDataAssemblerTest {

    /**
     * Test for toData method
     * Testing with transaction type debit
     */
    @Test
    void toDataWithTypeDebitTest() {
        //Arrange
        Type type = Type.DEBIT;
        Transaction transaction = new Transaction("1000",
                type,
                "2020-01-01 12:12:12",
                "description",
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));
        LedgerJpa ledgerJpa = new LedgerJpa("100");

        TransactionJpa expectedTransactionJpa = new TransactionJpa(ledgerJpa, new Amount("1000"),
                "2020-01-01 12:12:12",
                "Debit",
                new Description("description"),
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));
        //Act
        TransactionJpa actualTransactionJpa = TransactionDomainDataAssembler.toData(ledgerJpa, transaction);

        //Assert
        assertEquals(expectedTransactionJpa, actualTransactionJpa);
    }

    /**
     * Test for toData method
     * Testing with transaction type credit
     */
    @Test
    void toDataWithTypeCreditTest() {
        //Arrange
        Type type = Type.CREDIT;
        Transaction transaction = new Transaction("1000",
                type,
                "2020-01-01 12:12:12",
                "description",
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));
        LedgerJpa ledgerJpa = new LedgerJpa("100");


        TransactionJpa expectedTransactionJpa = new TransactionJpa(ledgerJpa, new Amount("1000"),
                "2020-01-01 12:12:12",
                "Credit",
                new Description("description"),
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));
        //Act
        TransactionJpa actualTransactionJpa = TransactionDomainDataAssembler.toData(ledgerJpa, transaction);

        //Assert
        assertEquals(expectedTransactionJpa, actualTransactionJpa);
    }

    /**
     * Test for toDomain method
     * Testing with transaction type debit
     */
    @Test
    void toDomainWithTypeDebitTest() {
        //Arrange
        LedgerJpa ledgerJpa = new LedgerJpa("100");
        TransactionJpa transactionJpa = new TransactionJpa(ledgerJpa, new Amount("1000"),
                "2020-01-01 12:12:12",
                "Debit",
                new Description("description"),
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));

        Type type = Type.DEBIT;
        Transaction expectedTransaction = new Transaction("1000",
                type,
                "2020-01-01 12:12:12",
                "description",
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));

        //Act
        Transaction actualTransaction = TransactionDomainDataAssembler.toDomain(transactionJpa);

        //Assert
        assertEquals(expectedTransaction, actualTransaction);
    }

    /**
     * Test for toDomain method
     * Testing with transaction type credit
     */
    @Test
    void toDomainWithTypeCreditTest() {
        //Arrange
        LedgerJpa ledgerJpa = new LedgerJpa("100");
        TransactionJpa transactionJpa = new TransactionJpa(ledgerJpa, new Amount("1000"),
                "2020-01-01 12:12:12",
                "Credit",
                new Description("description"),
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));

        Type type = Type.CREDIT;
        Transaction expectedTransaction = new Transaction("1000",
                type,
                "2020-01-01 12:12:12",
                "description",
                new Category("designation"),
                new AccountID("10"),
                new AccountID("20"));

        //Act
        Transaction actualTransaction = TransactionDomainDataAssembler.toDomain(transactionJpa);

        //Assert
        assertEquals(expectedTransaction, actualTransaction);
    }
}