package project.datamodel;

import org.junit.jupiter.api.Test;
import project.model.entities.account.Account;
import project.model.entities.shared.AccountID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class AccountDomainDataAssemblerTest {

    /**
     * Test for toData method
     * Happy Case
     */
    @Test
    void toDataHappyCaseTest() {
        //Arrange
        AccountID accountID = new AccountID("10");
        Account account = new Account(accountID, "denomination", "description");
        AccountJpa expectedAccountJpa = new AccountJpa("10", "denomination", "description");

        //Act
        AccountJpa actualAccountJpa = AccountDomainDataAssembler.toData(account);

        //Assert
        assertEquals(expectedAccountJpa, actualAccountJpa);

    }

    /**
     * Test for toData method
     * Ensure not equals test
     */
    @Test
    void toDataEnsureNotEqualsTest() {
        //Arrange
        AccountID accountID = new AccountID("10");
        Account account = new Account(accountID, "denomination", "description");
        AccountJpa expectedAccountJpa = new AccountJpa("20", "different denomination", "different description");

        //Act
        AccountJpa actualAccountJpa = AccountDomainDataAssembler.toData(account);

        //Assert
        assertNotEquals(expectedAccountJpa, actualAccountJpa);

    }

    /**
     * Test for toDomain method
     * Happy Case
     */
    @Test
    void toDomainHappyCaseTest() {
        AccountJpa accountJpa = new AccountJpa("10", "denomination", "description");
        AccountID accountID = new AccountID("10");
        Account expectedAccount = new Account(accountID, "denomination", "description");

        //Act
        Account actualAccount = AccountDomainDataAssembler.toDomain(accountJpa);

        //Assert
        assertEquals(expectedAccount, actualAccount);

    }

    /**
     * Test for toDomain method
     * Ensure not equals test
     */
    @Test
    void toDomainEnsureNotEqualsTest() {
        AccountJpa accountJpa = new AccountJpa("10", "denomination", "description");
        AccountID accountID = new AccountID("20");
        Account expectedAccount = new Account(accountID, "different denomination", "different description");

        //Act
        Account actualAccount = AccountDomainDataAssembler.toDomain(accountJpa);

        //Assert
        assertNotEquals(expectedAccount, actualAccount);

    }
}