package project.infrastructure.repositories.jpa;

import org.springframework.data.repository.CrudRepository;
import project.datamodel.LedgerJpa;
import project.model.entities.shared.LedgerID;

public interface LedgerJpaRepository extends CrudRepository<LedgerJpa, LedgerID> {
}