package project.exceptions;

import static project.exceptions.Messages.MEMBERALREADYEXISTS;

public class MemberAlreadyExistsException extends RuntimeException {
    static final long serialVersionUID = -6856015978634436074L;

    /**
     * Exception for when member already exists
     * Uses exceptions.Messages for exception message
     */
    public MemberAlreadyExistsException() {
        super(MEMBERALREADYEXISTS);
    }

    /**
     * Exception for when member already exists
     * Uses provided message for exception message
     */
    public MemberAlreadyExistsException(String message) {
        super(message);
    }
}
