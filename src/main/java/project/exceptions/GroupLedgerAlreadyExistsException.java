package project.exceptions;

import static project.exceptions.Messages.GROUPLEDGERALREADYEXISTS;

public class GroupLedgerAlreadyExistsException extends RuntimeException {
    static final long serialVersionUID = -7436624896423242517L;

    /**
     * Exception for when group ledger already exists
     * Uses exceptions.Messages for exception message
     */
    public GroupLedgerAlreadyExistsException() {
        super(GROUPLEDGERALREADYEXISTS);
    }

    /**
     * Exception for when group ledger already exists
     * Uses provided message for exception message
     */
    public GroupLedgerAlreadyExistsException(String message) {
        super(message);
    }
}
