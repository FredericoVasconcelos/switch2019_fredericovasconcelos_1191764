package project.exceptions;

import static project.exceptions.Messages.CATEGORYALREADYEXISTS;

public class CategoryAlreadyExistsException extends RuntimeException {
    static final long serialVersionUID = -1896588169795550066L;

    /**
     * Exception for when category already exists
     * Uses exceptions.Messages for exception message
     */
    public CategoryAlreadyExistsException() {
        super(CATEGORYALREADYEXISTS);
    }

    /**
     * Exception for when category already exists
     * Uses provided message for exception message
     */
    public CategoryAlreadyExistsException(String message) {
        super(message);
    }
}
