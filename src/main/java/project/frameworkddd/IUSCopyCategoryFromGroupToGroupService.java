package project.frameworkddd;

import project.dto.CopyCategoryFromGroupToGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;

public interface IUSCopyCategoryFromGroupToGroupService {

    CreateCategoryForGroupResponseDTO copyCategoryFromGroupToGroup(CopyCategoryFromGroupToGroupRequestDTO requestDTO);

}
