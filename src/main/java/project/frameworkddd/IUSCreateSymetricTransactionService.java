package project.frameworkddd;


import project.dto.CreateSymetricTransactionRequestDTO;
import project.dto.CreateSymetricTransactionResponseDTO;
import project.dto.PersonLedgersIDsDTO;

public interface IUSCreateSymetricTransactionService {
    CreateSymetricTransactionResponseDTO createSymetricTransaction(CreateSymetricTransactionRequestDTO requestDTO);

    PersonLedgersIDsDTO getLedgersByPersonID(String personID);
}
