package project.frameworkddd;

/**
 * Interface for an entity of any aggregate which is a pattern in Domain-Driven Design (DDD). Any entity has a life
 * cycle (can mutate in time) and is not defined by its attributes, but rather by its identity.
 *
 * @param <I> generic type which represent the id of the entity
 */
public interface Entity<I extends ValueObject> {

    @Override
    String toString();

    @Override
    boolean equals(Object other);

    @Override
    int hashCode();

    boolean sameAs(Object other);

    //compareTo

}
