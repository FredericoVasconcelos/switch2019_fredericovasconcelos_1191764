package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.*;
import project.frameworkddd.IUSCreateSymetricTransactionService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CreateSymetricTransactionRESTController {

    @Autowired
    private IUSCreateSymetricTransactionService service;

    /**
     * createTransactionForGroup method
     * <p>
     * Intends to create and add a new transaction to the ledger of a given group.
     * Method - POST
     * URI - /people/{personEmail}/groups/{groupID}/transactions
     * Path variables - personEmail and GroupID
     *
     * @param infoDTO     - Data Transfer Object that contains the necessary information to create a transaction
     * @param personEmail - The identity of the person member who's creating the group transaction
     * @return Response Entity with a body (a ResponseDTO or an error body in case an exception is thrown by the
     * application) and a Http status
     */
    @PostMapping("/people/{personEmail}/transactions")
    public ResponseEntity<Object> createSymetricTransaction(@RequestBody CreateSymetricTransactionInfoDTO infoDTO,
                                                            @PathVariable String personEmail) {

        CreateSymetricTransactionRequestDTO requestDTO = CreateSymetricTransactionAssembler.mapToRequestDTO(personEmail, infoDTO);

        CreateSymetricTransactionResponseDTO responseDTO = service.createSymetricTransaction(requestDTO);

//        Link transactionsLink =
//                linkTo(GetTransactionsRESTController.class).slash("people").slash(personEmail).slash("groups").slash(groupID).slash("transactions").withRel("group_transactions");
//        responseDTO.add(transactionsLink);

        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    /**
     * Method getLedgersByPersonID
     * Operates through RequestMapping of the Get type
     *
     * @param personID
     * @return
     */
    @GetMapping("/people/{personID}/ledgers")
    public ResponseEntity<Object> getLedgersByPersonID(@PathVariable final String personID) {

        PersonLedgersIDsDTO responseDTO = service.getLedgersByPersonID(personID);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
