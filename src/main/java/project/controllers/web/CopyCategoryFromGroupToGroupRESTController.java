package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.dto.CopyCategoryFromGroupToGroupAssembler;
import project.dto.CopyCategoryFromGroupToGroupInfoDTO;
import project.dto.CopyCategoryFromGroupToGroupRequestDTO;
import project.dto.CreateCategoryForGroupResponseDTO;
import project.frameworkddd.IUSCopyCategoryFromGroupToGroupService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class CopyCategoryFromGroupToGroupRESTController {

    @Autowired
    private IUSCopyCategoryFromGroupToGroupService service;

    /**
     * Method createCategoryForGroup
     *
     * @param info        an instance of CopyCategoryFromGroupToGroupInfoDTO which represents a DTO from the client
     * @param personEmail personEmail in the Path
     * @return If the group exists in the groupRepository, the person is manager of the group, but the category already exists: throws CategoryAlreadyExistsException - HttpStatus 422.
     */
    @PostMapping("/people/{personEmail}/groups/copy/category")
    public ResponseEntity<Object> copyCategoryFromGroupToGroup(@RequestBody CopyCategoryFromGroupToGroupInfoDTO info, @PathVariable String personEmail) {
        CopyCategoryFromGroupToGroupRequestDTO requestDTO = CopyCategoryFromGroupToGroupAssembler.mapToRequestDTO(personEmail, info.getGroupIdA(), info.getGroupIdB(), info.getDesignation());
        CreateCategoryForGroupResponseDTO result = service.copyCategoryFromGroupToGroup(requestDTO);
        Link selfLink = linkTo(methodOn(GetCategoriesRESTController.class).getCategoriesByGroupID(info.getGroupIdB())).withSelfRel();
        result.add(selfLink);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

}
