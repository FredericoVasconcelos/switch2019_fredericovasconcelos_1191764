package project.controllers.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import project.dto.GetPersonInfoResponseDTO;
import project.frameworkddd.IUSGetPersonInfoService;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class GetPersonInfoRESTController {

    @Autowired
    IUSGetPersonInfoService service;

    /**
     * Method getPersonByID
     * Operates through RequestMapping of the Get type
     *
     * @param personID
     * @return
     */
    @GetMapping("/people/{personID}")
    public ResponseEntity<Object> getPersonInfoByID(@PathVariable final String personID) {

        GetPersonInfoResponseDTO result = service.getPersonInfo(personID);

        // Add links for persons groups
        Link linkGroups = linkTo(GetPersonsGroupsRESTController.class).slash("people").slash(personID).slash("groups").withRel("groupsURI");
        result.add(linkGroups);

        // Add links for persons accounts
        Link linkAccounts = linkTo(CreateAccountForPersonRESTController.class).slash("people").slash(personID).slash("accounts").withRel("accountsURI");
        result.add(linkAccounts);

        // Add links for persons categories
        Link linkCategories = linkTo(GetCategoriesRESTController.class).slash("people").slash(personID).slash("categories").withRel("categoriesURI");
        result.add(linkCategories);

        // Add links for persons transactions
        Link linkTransactions = linkTo(GetTransactionsRESTController.class).slash("people").slash(personID).slash("transactions").withRel("transactionsURI");
        result.add(linkTransactions);

        // Add link to create a new group
        Link linkCreateGroup = linkTo(CreateGroupRESTController.class).slash("people").slash(personID).slash("groups").withRel("createGroupURI");
        result.add(linkCreateGroup);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}