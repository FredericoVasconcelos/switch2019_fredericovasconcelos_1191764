package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.CopyCategoryFromGroupToGroupRequestDTO;
import project.dto.CreateCategoryForGroupAssembler;
import project.dto.CreateCategoryForGroupResponseDTO;
import project.exceptions.CategoryNotFoundException;
import project.exceptions.GroupNotFoundException;
import project.exceptions.PersonIsNotManagerOfTheGroupException;
import project.exceptions.PersonNotFoundException;
import project.frameworkddd.IUSCopyCategoryFromGroupToGroupService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.person.Person;
import project.model.entities.shared.Category;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.Optional;

@Service
public class CopyCategoryFromGroupToGroupService implements IUSCopyCategoryFromGroupToGroupService {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    GroupRepository groupRepository;

    /**
     * Method copyCategoryFromGroupToGroup
     * Add a Category to categories of the group
     * verify if groupRepository contains groupID
     *
     * @param requestDTO CopyCategoryFromGroupToGroupRequestDTO
     * @return If the group exists in the groupRepository, the person is manager of the group, but the category already exists: throws CategoryAlreadyExistsException
     */
    @Override
    public CreateCategoryForGroupResponseDTO copyCategoryFromGroupToGroup(CopyCategoryFromGroupToGroupRequestDTO requestDTO) {
        Person person = getPersonByID(requestDTO.getPersonEmail());

        Group groupA = getGroupByID(requestDTO.getGroupIDA());
        hasManagerID(groupA, person);
        hasCategory(groupA, requestDTO.getDesignation());

        Group groupB = getGroupByID(requestDTO.getGroupIDB());
        hasManagerID(groupB, person);

        String designation = requestDTO.getDesignation();
        groupB.addCategory(designation);

        Group savedGroupB = groupRepository.save(groupB);
        return CreateCategoryForGroupAssembler.mapToResponseDTO(
                savedGroupB.getID().toStringDTO(),
                savedGroupB.getDescription().getDescriptionValue(),
                designation);

    }

    /**
     * Method to get person from given ID with verification
     *
     * @param personID ID of person
     * @return person
     */
    private Person getPersonByID(String personID) {
        Optional<Person> optPerson = personRepository.findById(new PersonID(personID));

        if (!optPerson.isPresent()) {
            throw new PersonNotFoundException();
        } else {
            return optPerson.get();
        }
    }

    /**
     * Method to get group from given ID with verification
     *
     * @param groupID ID of group
     * @return person
     */
    private Group getGroupByID(String groupID) {
        Optional<Group> optGroup = groupRepository.findById(new GroupID(groupID));

        if (!optGroup.isPresent()) {
            throw new GroupNotFoundException();
        } else {
            return optGroup.get();
        }
    }

    /**
     * Check if person is admin of group
     *
     * @param group
     * @param person
     */
    private void hasManagerID(Group group, Person person) {
        if (!group.hasManagerID(person.getID())) {
            throw new PersonIsNotManagerOfTheGroupException();
        }
    }

    /**
     * Check if group has category
     *
     * @param group
     * @param designation
     */
    private void hasCategory(Group group, String designation) {
        Category categoryToCopy = new Category(designation);

        if (!group.getCategories().hasCategory(categoryToCopy)) {
            throw new CategoryNotFoundException();
        }
    }

}