package project.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.dto.*;
import project.exceptions.*;
import project.frameworkddd.IUSCreateSymetricTransactionService;
import project.model.entities.group.Group;
import project.model.entities.group.GroupID;
import project.model.entities.ledger.Ledger;
import project.model.entities.ledger.Type;
import project.model.entities.person.Person;
import project.model.entities.shared.AccountID;
import project.model.entities.shared.Category;
import project.model.entities.shared.LedgerID;
import project.model.entities.shared.PersonID;
import project.model.specifications.repositories.AccountRepository;
import project.model.specifications.repositories.GroupRepository;
import project.model.specifications.repositories.LedgerRepository;
import project.model.specifications.repositories.PersonRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CreateSimetricTransactionService implements IUSCreateSymetricTransactionService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private LedgerRepository ledgerRepository;

    @Autowired
    private AccountRepository accountRepository;

    /**
     * createTransactionForGroup method
     * <p>
     * This method intends to create a simetric transaction and add it to given ledger.
     * Throws exceptions with the following scenarios:
     * - PersonEmail doesn't exist in PersonRepository
     * - LedgerA doesn't exist in LedgerRepository
     * - LedgerB doesn't exist in LedgerRepository
     * - The credit account doesn't exist in AccountRepository
     * - The debit account doesn't exist in AccountRepository
     * - The input type of the transaction is invalid
     *
     * @param requestDTO a data transfer object with the necessary information to create a transaction for the group
     * @return a response DTO with some information regarding the created transaction if successfully created
     */
    @Override
    public CreateSymetricTransactionResponseDTO createSymetricTransaction(CreateSymetricTransactionRequestDTO requestDTO) {

        Optional<Person> personOpt = personRepository.findById(new PersonID(requestDTO.getPersonEmail()));
        if (!personOpt.isPresent())
            throw new PersonNotFoundException();

        Person person = personOpt.get();

        LedgerID ledgerIDA = new LedgerID(requestDTO.getLedgerIDA());
        if (!ledgerRepository.findById(ledgerIDA).isPresent())
            throw new LedgerNotFoundException("Ledger A not found");

        LedgerID ledgerIDB = new LedgerID(requestDTO.getLedgerIDB());
        Optional<Ledger> ledgerOpt = ledgerRepository.findById(ledgerIDB);
        if (!ledgerRepository.findById(ledgerIDB).isPresent())
            throw new LedgerNotFoundException("Ledger B not found");

        AccountID debitAccountId = new AccountID(requestDTO.getCreditAccountID());
        if (!accountRepository.existsById(debitAccountId))
            throw new AccountNotFoundException("Debit account not found");

        AccountID creditAccountId = new AccountID(requestDTO.getDebitAccountID());
        if (!accountRepository.existsById(creditAccountId))
            throw new AccountNotFoundException("Credit account not found");

        Category category = new Category(requestDTO.getCategoryDesignation());
        if (!person.getCategories().hasCategory(category))
            throw new CategoryNotFoundException();

        Type type;
        if (requestDTO.getType() == "1") {
            type = Type.DEBIT;
        } else {
            type = Type.CREDIT;
        }


        Ledger ledger = ledgerOpt.get();
        ledger.addTransaction(requestDTO.getAmount(),
                type,
                requestDTO.getDateTime(),
                requestDTO.getDescription(),
                new Category(requestDTO.getCategoryDesignation()),
                new AccountID(debitAccountId.toStringDTO()),
                new AccountID(creditAccountId.toStringDTO()));

        ledgerRepository.save(ledger);

        return CreateSymetricTransactionAssembler.mapToResponseDTO(ledger.getID().toStringDTO(),
                requestDTO.getAmount(), requestDTO.getType(),
                requestDTO.getDescription());
    }

    /**
     * Method getLedgersPersonID
     * Uses internal method getPersonsLedgersIDs
     *
     * @param personID ID of person
     */
    @Override
    public PersonLedgersIDsDTO getLedgersByPersonID(String personID) {

        Optional<Person> personOpt = personRepository.findById(new PersonID(personID));
        if (!personOpt.isPresent()) {
            throw new PersonNotFoundException();
        }

        Person person = personOpt.get();

        List<String> allOwnedLedgersIDs = getPersonsLedgersIDs(person);

        return CreateSymetricTransactionAssembler.mapToPersonLedgerIDsDTO(allOwnedLedgersIDs);
    }

    /**
     * Method to get all of user's LedgerIDs
     * Uses internal method getPersonsManagedGroupsLedgersIDs
     *
     * @param person user making the request
     */
    private List<String> getPersonsLedgersIDs(Person person) {
        List<String> personLedgersIDs = new ArrayList<String>();

        String personLedgerID = person.getLedgerID().toStringDTO();
        personLedgersIDs.add(personLedgerID);

        List<String> personsManagedGroupsLedgersIDs = getPersonsManagedGroupsLedgersIDs(person.getID());

        personLedgersIDs.addAll(personsManagedGroupsLedgersIDs);

        return personLedgersIDs;
    }

    /**
     * Method to get all user's managed groups LedgersIDs
     * @param personID user making the request
     */
    private List<String> getPersonsManagedGroupsLedgersIDs(PersonID personID) {
        List<String> personsGroupsLedgersIDs = new ArrayList<String>();

        for (Group group : groupRepository.findAll()) {
            if (group.hasManagerID(personID)) {
                personsGroupsLedgersIDs.add(group.getLedgerID().toStringDTO());
            }
        }
        return personsGroupsLedgersIDs;
    }
}
