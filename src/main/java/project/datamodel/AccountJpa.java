package project.datamodel;

import lombok.Data;
import lombok.NoArgsConstructor;
import project.model.entities.shared.AccountID;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "accounts")
public class AccountJpa {

    @EmbeddedId
    private AccountID id;

    private String denomination;
    private String description;

    public AccountJpa(String id, String denomination, String description) {
        this.id = new AccountID(id);
        this.denomination = denomination;
        this.description = description;
    }

    public AccountJpa(AccountID id, String denomination, String description) {
        this.id = id;
        this.denomination = denomination;
        this.description = description;
    }

    @Override
    public String toString() {
        return "AccountJpa{" +
                "id=" + id +
                ", denomination='" + denomination + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
