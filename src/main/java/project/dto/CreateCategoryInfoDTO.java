package project.dto;

import java.util.Objects;

public class CreateCategoryInfoDTO {

    private String designation;

    /**
     * Constructor for CreateCategoryForGroupInfoDTO
     *
     * @param designation designation of category
     */
    public CreateCategoryInfoDTO(String designation) {
        setDesignation(designation);
    }

    /**
     * Constructor for CreateCategoryForGroupInfoDTO
     */
    CreateCategoryInfoDTO() {
        //Added because of the dependency injection of the framework spring. Needs a empty constructor.
    }

    /**
     * method for getDesignation
     *
     * @return designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * method setDesignation
     *
     * @param designation
     */
    private void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateCategoryInfoDTO that = (CreateCategoryInfoDTO) o;
        return Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation);
    }
}
