package project.dto;

public final class CopyCategoryFromGroupToGroupAssembler {

    /**
     * Constructor for CreateCategoryForGroupAssembler
     * Created to override the public constructor Java creates automatically if there is not a constructor for a class
     */
    CopyCategoryFromGroupToGroupAssembler() {
        //Intentionally empty
    }

    /**
     * Method mapToDtoRequest
     *
     * @param personEmail personEmail
     * @param groupIDA    groupIDA
     * @param groupIDB    groupIDB
     * @param designation designation of category
     * @return instance of CreateCategoryForGroupRequestDTO
     */
    public static CopyCategoryFromGroupToGroupRequestDTO mapToRequestDTO(String personEmail, String groupIDA, String groupIDB, String designation) {
        return new CopyCategoryFromGroupToGroupRequestDTO(personEmail, groupIDA, groupIDB, designation);
    }

}
