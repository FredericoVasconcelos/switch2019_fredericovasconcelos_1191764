package project.dto;

import java.util.Objects;

public class CopyCategoryFromGroupToGroupRequestDTO {

    private String personEmail;
    private String groupIDA;
    private String groupIDB;
    private String designation;

    /**
     * Constructor for CopyCategoryFromGroupToGroupRequestDTO
     *
     * @param groupIDA    groupIDA
     * @param designation designation of the Category
     */
    public CopyCategoryFromGroupToGroupRequestDTO(String personEmail, String groupIDA, String groupIDB, String designation) {
        setPersonEmail(personEmail);
        setGroupIDA(groupIDA);
        setGroupIDB(groupIDB);
        setDesignation(designation);
    }

    /**
     * Method getPersonID
     *
     * @return
     */
    public String getPersonEmail() {
        return personEmail;
    }

    /**
     * Method setPersonID
     *
     * @param personEmail
     */
    private void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    /**
     * Method getGroupIDA
     *
     * @return groupIDA
     */
    public String getGroupIDA() {
        return groupIDA;
    }

    /**
     * Method setGroupIDA
     *
     * @param groupIDA groupIDA
     */
    private void setGroupIDA(String groupIDA) {
        this.groupIDA = groupIDA;
    }

    /**
     * Method getGroupIDB
     *
     * @return groupIDB
     */
    public String getGroupIDB() {
        return groupIDB;
    }

    /**
     * Method setGroupIDB
     *
     * @param groupIDB groupIDB
     */
    private void setGroupIDB(String groupIDB) {
        this.groupIDB = groupIDB;
    }

    /**
     * Method getDesignation
     *
     * @return designation of Category
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Method setDesignation
     *
     * @param designation designation of Category
     */
    private void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CopyCategoryFromGroupToGroupRequestDTO that = (CopyCategoryFromGroupToGroupRequestDTO) o;
        return Objects.equals(personEmail, that.personEmail) &&
                Objects.equals(groupIDA, that.groupIDA) &&
                Objects.equals(groupIDB, that.groupIDB) &&
                Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personEmail, groupIDA, groupIDB, designation);
    }
}
