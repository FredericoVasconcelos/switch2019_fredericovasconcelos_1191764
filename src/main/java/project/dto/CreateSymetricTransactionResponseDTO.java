package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;

public class CreateSymetricTransactionResponseDTO extends RepresentationModel<CreateSymetricTransactionResponseDTO> {

    final String ledgerIDB;
    final String amount;
    final String type;
    final String transactionDescription;

    /**
     * Constructor of CreateTransactionForGroupResponseDTO class
     *
     * @param ledgerIDB              The identity of the ledger to which the simetric transaction was created
     * @param amount                 The amount of the created transaction
     * @param type                   The type of the created transaction
     * @param transactionDescription The description of the created transaction
     */
    public CreateSymetricTransactionResponseDTO(String ledgerIDB, String amount, String type,
                                                String transactionDescription) {

        this.ledgerIDB = ledgerIDB;
        this.amount = amount;
        this.type = type;
        this.transactionDescription = transactionDescription;
    }

    /**
     * Method getGroupID
     * gets the groupID attribute from responseDTO
     *
     * @return groupID as a String
     */
    public String getLedgerIDB() {
        return ledgerIDB;
    }

    /**
     * Method getAmount
     * gets the amount attribute from responseDTO
     *
     * @return amount as a String
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Method getType
     * gets the transaction type attribute from responseDTO
     *
     * @return type as a String
     */
    public String getType() {
        return type;
    }

    /**
     * Method getTransactionDescription
     * gets the transactionDescription attribute from responseDTO
     *
     * @return transactionDescription as a String
     */
    public String getTransactionDescription() {
        return transactionDescription;
    }

    /**
     * Override of equals method
     *
     * @param o Object to be compared to this
     * @return True if this is equal to Object o
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        CreateSymetricTransactionResponseDTO that = (CreateSymetricTransactionResponseDTO) o;
        return Objects.equals(ledgerIDB, that.ledgerIDB) &&
                Objects.equals(amount, that.amount) &&
                Objects.equals(type, that.type) &&
                Objects.equals(transactionDescription, that.transactionDescription);
    }

    /**
     * Override of hashCode method
     *
     * @return Integer hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), ledgerIDB, amount, type, transactionDescription);
    }
}
