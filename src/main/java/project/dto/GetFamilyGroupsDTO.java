package project.dto;

import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;

public class GetFamilyGroupsDTO extends RepresentationModel<GetFamilyGroupsDTO> {

    private Set<String> familyGroupsIDs;

    /**
     * Constructor for GetFamilyGroupsDTO
     *
     * @param groupsIDs
     */
    public GetFamilyGroupsDTO(Set<String> groupsIDs) {
        setFamilyGroupsIDs(groupsIDs);
    }

    /**
     * Get GroupsIDs
     *
     * @return Set <strings>
     */
    public Set<String> getFamilyGroupsIDs() {
        return Collections.unmodifiableSet(familyGroupsIDs);
    }

    /**
     * setGroupsIDs
     *
     * @param familyGroupsIDs
     */
    private void setFamilyGroupsIDs(Set<String> familyGroupsIDs) {
        this.familyGroupsIDs = Collections.unmodifiableSet(familyGroupsIDs);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetFamilyGroupsDTO)) return false;
        GetFamilyGroupsDTO that = (GetFamilyGroupsDTO) o;
        return Objects.equals(familyGroupsIDs, that.familyGroupsIDs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(familyGroupsIDs);
    }
}
