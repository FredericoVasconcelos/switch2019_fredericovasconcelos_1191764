package project.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Collections;
import java.util.List;

@Getter
@EqualsAndHashCode(callSuper = false)
public class PersonLedgersIDsDTO extends RepresentationModel<PersonLedgersIDsDTO> {

    private final List<String> ownedLedgersIDs;

    /**
     * Constructor of PersonLedgerIDsDTO
     * @param ownedLedgersIDs list of LedgerIDs owned by user as String
     */
    public PersonLedgersIDsDTO(List<String> ownedLedgersIDs) {
        this.ownedLedgersIDs = Collections.unmodifiableList(ownedLedgersIDs);
    }

    @Override
    public String toString() {
        return "PersonLedgerIDsDTO{" +
                "ownedLedgersIDs=" + ownedLedgersIDs +
                '}';
    }
}
