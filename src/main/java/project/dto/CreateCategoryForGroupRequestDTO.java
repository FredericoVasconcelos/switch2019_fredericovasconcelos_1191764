package project.dto;

import java.util.Objects;

public class CreateCategoryForGroupRequestDTO {

    private String personEmail;
    private String groupID;
    private String designation;

    /**
     * Constructor for CreateCategoryForGroupRequestDTO
     *
     * @param groupID     groupID
     * @param designation designation of the Category
     */
    public CreateCategoryForGroupRequestDTO(String personEmail, String groupID, String designation) {
        setPersonEmail(personEmail);
        setGroupID(groupID);
        setDesignation(designation);
    }

    /**
     * Method getPersonID
     *
     * @return
     */
    public String getPersonEmail() {
        return personEmail;
    }

    /**
     * Method setPersonID
     *
     * @param personEmail
     */
    private void setPersonEmail(String personEmail) {
        this.personEmail = personEmail;
    }

    /**
     * Method getGroupID
     *
     * @return groupID
     */
    public String getGroupID() {
        return groupID;
    }

    /**
     * Method setGroupID
     *
     * @param groupID groupID
     */
    private void setGroupID(String groupID) {
        this.groupID = groupID;
    }

    /**
     * Method getDesignation
     *
     * @return designation of Category
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Method setDesignation
     *
     * @param designation designation of Category
     */
    private void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateCategoryForGroupRequestDTO that = (CreateCategoryForGroupRequestDTO) o;
        return Objects.equals(personEmail, that.personEmail) &&
                Objects.equals(groupID, that.groupID) &&
                Objects.equals(designation, that.designation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personEmail, groupID, designation);
    }
}
