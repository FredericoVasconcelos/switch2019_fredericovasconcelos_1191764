package project.dto;

import java.util.List;

public final class CreateSymetricTransactionAssembler {

    /**
     * Constructor of CreateTransactionForGroupAssembler
     */
    CreateSymetricTransactionAssembler() {
        //Contructor is intentionally empty
    }

    /**
     * Method mapToRequestDTO
     * maps the information passed to the controller (Request Body as an infoDTO and path variables) into a new DTO with
     * the information needed by the CreateTransactionForGroupService to create a new transaction.
     *
     * @param personEmail The identity of the group member who is creating the new transaction as a String
     * @param infoDTO     Aggregates the remaining needed information to create a transaction
     * @return DTO with all the information to create a group transaction to be passed to the service
     */
    public static CreateSymetricTransactionRequestDTO mapToRequestDTO(String personEmail,
                                                                      CreateSymetricTransactionInfoDTO infoDTO) {

        return new CreateSymetricTransactionRequestDTO(personEmail, infoDTO.getLedgerIDA(), infoDTO.getLedgerIDB(),
                infoDTO.getAmount(), infoDTO.getDateTime(), infoDTO.getType(), infoDTO.getDescription(),
                infoDTO.getCategoryDesignation(), infoDTO.getDebitAccountID(), infoDTO.getCreditAccountID());
    }

    /**
     * Method mapToResponseDTO
     * maps the information that is supposed to return to the presentation layer when a new group transaction is created
     * inside a new DTO
     *
     * @param ledgerIDB              The identity of the ledger to which the symmetric transaction was created
     * @param amount                 The amount of the created transaction
     * @param type                   The type of the created transaction
     * @param transactionDescription The description of the created transaction
     * @return DTO with the information related to the created transaction to be returned to the presentation layer
     */
    public static CreateSymetricTransactionResponseDTO mapToResponseDTO(String ledgerIDB, String amount, String type,
                                                                        String transactionDescription) {

        return new CreateSymetricTransactionResponseDTO(ledgerIDB, amount, type, transactionDescription);
    }

    /**
     * Method mapToPersonLedgerIDsDTO
     *
     * @param ownedLedgersIDs IDs of all ledgers owned by user encapsulated in a DTO as a list of Strings
     * @return
     */
    public static PersonLedgersIDsDTO mapToPersonLedgerIDsDTO(List<String> ownedLedgersIDs) {
        return new PersonLedgersIDsDTO(ownedLedgersIDs);
    }

}