package project.dto;

import java.util.Objects;

public class CopyCategoryFromGroupToGroupInfoDTO {

    private String designation;
    private String groupIdA;
    private String groupIdB;

    /**
     * Constructor for CopyCategoryFromGroupToGroupInfoDTO
     *
     * @param designation designation of category
     */
    public CopyCategoryFromGroupToGroupInfoDTO(String designation, String groupIdA, String groupIdB) {
        setDesignation(designation);
        setGroupIdA(groupIdA);
        setGroupIdB(groupIdB);
    }

    /**
     * Constructor for CreateCategoryForGroupInfoDTO
     */
    CopyCategoryFromGroupToGroupInfoDTO() {
        //Added because of the dependency injection of the framework spring. Needs a empty constructor.
    }
    /**
     * method for getGroupIdA
     *
     * @return groupIdA
     */
    public String getGroupIdA() {
        return groupIdA;
    }

    /**
     * method setGroupIdA
     *
     * @param groupIdA
     */
    public void setGroupIdA(String groupIdA) {
        this.groupIdA = groupIdA;
    }
    /**
     * method for getGroupIdB
     *
     * @return groupIdB
     */
    public String getGroupIdB() {
        return groupIdB;
    }

    /**
     * method setGroupIdB
     *
     * @param groupIdB
     */
    public void setGroupIdB(String groupIdB) {
        this.groupIdB = groupIdB;
    }

    /**
     * method for getDesignation
     *
     * @return designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * method setDesignation
     *
     * @param designation
     */
    private void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CopyCategoryFromGroupToGroupInfoDTO that = (CopyCategoryFromGroupToGroupInfoDTO) o;
        return Objects.equals(designation, that.designation) &&
                Objects.equals(groupIdA, that.groupIdA) &&
                Objects.equals(groupIdB, that.groupIdB);
    }

    @Override
    public int hashCode() {
        return Objects.hash(designation, groupIdA, groupIdB);
    }
}
