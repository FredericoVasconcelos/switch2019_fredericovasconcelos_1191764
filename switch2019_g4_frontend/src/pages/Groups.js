import React from "react";
import ExpansionPanel from "../components_experiments/ExpansionPanel";
import HeaderBar from "../components_experiments/GroupsHeaderBar";
import GroupData from "../components_experiments/GroupData";

function Groups() {

    return (
        <div style={{marginLeft: "10%", marginRight: "10%"}}>
            <br/>
            <HeaderBar/>
            <br/>
            <GroupData/>
            <br/>
            <ExpansionPanel name={"MEMBERS"} aggregate='groupMembers'/>
            <br/>
            <ExpansionPanel name={"ACCOUNTS"} aggregate='accountsGroup'/>
            <br/>
            <ExpansionPanel name={"CATEGORIES"} aggregate='categoriesGroup'/>
            <br/>
            <ExpansionPanel name={"TRANSACTIONS"} aggregate='transactionsGroup'/>
        </div>
    )
}

export default Groups