import React, {useContext} from "react";
import PersonContext from "../context/PersonContext";

function TableGroupsBody() {
    const {state} = useContext(PersonContext);
    const {groups} = state;
    const {data} = groups;

    const rows = data.map((row, index) => {
        return (
            <tr key={index}>
                <td>{row.groupID}</td>
                <td>{row.description}</td>
                <td>{row.creationDate}</td>
            </tr>

        )
    });

    return (
        <tbody>{rows}</tbody>
    )
}

export default TableGroupsBody