import React, {useContext, useState} from "react";
import {fetchGroupTransactionsAfterPost, URL_API} from "../context/GroupActions";
import AppContext from "../context/AppContext";
import PersonContext from "../context/PersonContext";
import GroupContext from "../context/GroupContext";
import SnackbarContext from "../context/SnackbarContext";
import {
    showErrorSnackbar,
    showSuccessfulSnackbar,
    TRANSACTION_CREATION_FAILED,
    TRANSACTION_SUCCESSFULLY_CREATED
} from "../context/SnackbarActions";
import Select from '@material-ui/core/Select';
import Input from '@material-ui/core/Input';

function FormTransactionsGroup() {

    const {username} = useContext(AppContext).state;
    const {groupId} = useContext(PersonContext).state;
    const categories = useContext(GroupContext).state.categories.data;
    const accounts = useContext(GroupContext).state.accounts.data;
    const {dispatch} = useContext(GroupContext);
    const snackDispatch = useContext(SnackbarContext).dispatch;

    const [inputData, setInputData] = useState({
        amount: null,
        dateTime: null,
        type: "debit",
        description: "",
        categoryDesignation: "",
        debitAccountID: "",
        creditAccountID: "",
    });

    function handleChange(event) {
        const {name, value} = event.target;
        setInputData(prevInputData => {
            return {
                ...prevInputData,
                [name]: value
            }
        })
    }

    function handleSubmit(event) {
        event.preventDefault();
        fetch(`${URL_API}/people/${username}/groups/${groupId}/transactions`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(inputData)
        }).then(res => res.json()).then(res => {
            res.message === undefined ?
                snackDispatch(showSuccessfulSnackbar(TRANSACTION_SUCCESSFULLY_CREATED)) :
                snackDispatch(showErrorSnackbar(TRANSACTION_CREATION_FAILED + res.message));
            console.log(res)
        })
            .catch(error => {
                console.log(error);
                snackDispatch(showErrorSnackbar(TRANSACTION_CREATION_FAILED + error.message))
            }).then(() => {
            setInputData(prevInputData => {
                return {
                    amount: null,
                    dateTime: null,
                    type: "debit",
                    description: "",
                    categoryDesignation: "",
                    debitAccountID: "",
                    creditAccountID: "",
                }
            });
            dispatch(fetchGroupTransactionsAfterPost())
        })

    }

    return (
        <form onSubmit={handleSubmit} id="transactionGroupForm">
            <div className="ModalHeader">
                <p>Create new transaction</p>
            </div>
            <div>
                <label className="labelForm">Debit Account:</label><br/>
                <Select
                    native
                    name="debitAccountID"
                    onChange={handleChange}
                    required>
                    {accounts.map(({accountID, denomination}) => (
                        <option value={accountID}>
                            {accountID.concat(" - " + denomination)}
                        </option>
                    ))}
                    <option value="" selected disabled hidden></option>
                </Select>
            </div>
            <br/>
            <div>
                <label className="labelForm">Credit Account:</label><br/>
                <Select
                    native
                    name="creditAccountID"
                    onChange={handleChange}
                    required>
                    {accounts.map(({accountID, denomination}) => (
                        <option value={accountID}>
                            {accountID.concat(" - " + denomination)}
                        </option>
                    ))}
                    <option value="" selected disabled hidden></option>
                </Select>
            </div>
            <br/>
            <div>
                <label className="labelForm" htmlFor="amount">Amount:</label><br/>
                <Input type="number" name="amount" value={inputData.amount}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <div>
                <label className="labelForm" htmlFor="category">Category:</label><br/>
                <Select
                    native
                    name="categoryDesignation"
                    onChange={handleChange} required>
                    {categories.map(({designation}) => (
                        <option value={designation}>
                            {designation}
                        </option>
                    ))}
                    <option value="" selected disabled hidden></option>
                </Select>
            </div>
            <br/>
            <div>
                <label className="labelForm" htmlFor="description">Description:</label><br/>
                <Input type="text" name="description" value={inputData.description}
                       onChange={handleChange} required/>
            </div>
            <br/>
            <button className="buttonForm" id="submitGroupTransaction">Submit</button>
        </form>
    )

}

export default FormTransactionsGroup;