import React from 'react';
import ReactDOM from 'react-dom';
import FormAccounts from "./FormAccounts";
import FormGroups from "./FormGroups";
import FormCategories from "./FormCategories";
import FormGroupMembers from "./FormGroupMembers";
import FormTransactionsGroup from "./FormTransactionsGroup";
import FromGroupCategories from "./FormGroupCategories";
import FormGroupAccounts from "./FormGroupAccounts";

function chooseForm(aggregate) {
    if (aggregate === 'accounts') {
        return (
            <FormAccounts/>
        )
    } else if (aggregate === 'groups') {
        return (
            <FormGroups/>
        )
    } else if (aggregate === 'categories') {
        return (
            <FormCategories/>
        )
    } else if (aggregate === 'members') {
        return (
            <FormGroupMembers/>
        )
    } else if (aggregate === 'groupAccounts') {
        return (
            <FormGroupAccounts/>
        )
    } else if (aggregate === 'transactionsGroup') {
        return (
            <FormTransactionsGroup/>
        )
    } else if (aggregate === 'CategoriesGroup') {
        return (
            <FromGroupCategories/>
        )
    }
}

const Modal = ({isShowing, hide, aggregate}) => isShowing ? ReactDOM.createPortal(
    <React.Fragment>
        <div className="modal-overlay"/>
        <div className="modal-wrapper" aria-modal aria-hidden tabIndex={-1} role="dialog" onSubmit={hide}>
            <div className="modal">
                <div className="modal-header">
                    <button type="button" className="modal-close-button" data-dismiss="modal" aria-label="Close"
                            onClick={hide}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {chooseForm(aggregate)}
            </div>
        </div>
    </React.Fragment>, document.body
) : null;

export default Modal;