import React, {useContext} from "react";
import PersonContext from "../context/PersonContext";

function TableGroupsHeader() {
    const {groupsHeaders} = useContext(PersonContext);
    const {id, description, creationDate} = groupsHeaders;

    return (
        <thead>
        <tr>
            <th style={{width: "50px"}}>{id}></th>
            <th style={{width: "500px"}}>{description}></th>
            <th style={{width: "200px"}}>{creationDate}></th>
        </tr>
        </thead>
    )
}

export default TableGroupsHeader