import Snackbar from "@material-ui/core/Snackbar";
import React, {useContext} from "react";
import SnackbarContext from "../context/SnackbarContext";
import {clearSnackbar} from "../context/SnackbarActions";
import Alert from "@material-ui/lab/Alert";
import Slide from "@material-ui/core/Slide";

export default function AppSnackbar() {

    const {state, dispatch} = useContext(SnackbarContext);
    const {snackbarOpened, message, severity} = state;

    const SlideTransition = props => <Slide {...props} direction='up'/>;

    function handleClose() {
        dispatch(clearSnackbar());
    }

    return (

        <div>
            <Snackbar
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
                open={snackbarOpened}
                autoHideDuration={5000}
                onClose={handleClose}
                TransitionComponent={SlideTransition}
            >
                <Alert
                    onClose={handleClose}
                    severity={severity}
                >
                    {message}
                </Alert>
            </Snackbar>
        </div>

    );
}