import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;
//====================================================================================
//este projeto contém o ChromeDriver 83.0.4103.39 (https://chromedriver.chromium.org/)
//na pasta raiz do projeto
//====================================================================================

public class TestSelenium {
    static WebDriver driver;

    @BeforeAll
    public static void setUpBeforeAll() {
        //definir o driver e caminho + instanciar o driver
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        //definir 2 segundos de espera para carregar páginas,
        //senão pode não funcionar porque a informação pode ainda não ter sido renderizada
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

    //=========================================
    @AfterAll
    public static void setUpAfterAll() {
        driver.close(); //fecha o TAB usado
        driver.quit();  //fecha o browser usado
    }

    //=========================================
    @Test
    void testSignIn() {
        // mostra a pagina
        // inserir email
        // + clica no botão "Sign In"
        // + verifica se aparece o texto formatado "diogo12@switch.pt"

        driver.get("http://localhost:3000/");

        driver.findElement(By.id("email")).sendKeys("1001@switch.pt");
        driver.findElement(By.className("MuiButton-label")).click();
        String txt = driver.findElement(By.linkText("1001@switch.pt")).getText();
        Assertions.assertEquals("1001@switch.pt", txt);
    }

//    @Test
//    void testAddGroup() {
//        // mostra a pagina
//        // inserir email
//        // + clica no botão "Sign In"
//        // + clicar no botao "LOGOUT"
//
//        driver.get("http://localhost:3000/");
//
//        driver.findElement(By.id("email")).sendKeys("1001@switch.pt");
//        driver.findElement(By.className("MuiButton-label")).click(); // sign in button
//        driver.findElement(By.xpath("//h4[contains(text(), '" + "GROUPS" + "')]")).click();
//        driver.findElement(By.xpath("//button[@title='Add Group']")).click();
//        driver.switchTo().activeElement();
//        driver.findElement(By.id("groupForm")).findElement(By.name("groupID")).sendKeys("999999");
//        driver.findElement(By.id("groupForm")).findElement(By.name("description")).sendKeys("999999");
//        driver.findElement(By.id("submitGroup")).click();
//        driver.findElement(By.className("modal-close-button")).click();
//        driver.switchTo().activeElement();
//        String txt = driver.findElement(By.className("snackbar")).getText();
//        Assertions.assertEquals("Failed to create new group - Group already exists", txt);
//
//    }

    @Test
    void testLogout() {
        // mostra a pagina
        // inserir email
        // + clica no botão "Sign In"
        // + clicar no botao "LOGOUT"

        driver.get("http://localhost:3000/");

        driver.findElement(By.id("email")).sendKeys("1001@switch.pt");
        driver.findElement(By.className("MuiButton-label")).click(); // sign in button
        driver.findElement(By.className("MuiButton-label")).click(); // LOGOUT button
        String txt = driver.findElement(By.className("MuiButton-label")).getText();
        Assertions.assertEquals("SIGN IN", txt);

    }
}
